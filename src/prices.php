<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>DSYH: Dont Scratch Your Head</title>

        <link rel="stylesheet" href="css/tanzy-toast.css">
        <script src="js/tanzy-toast.js"></script>
        <!--ChatBot-->
        <script>
            /*<![CDATA[*/
            window.zEmbed || function (e, t) {
                var n, o, d, i, s, a = [], r = document.createElement("iframe");
                window.zEmbed = function () {
                    a.push(arguments)
                }, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
                try {
                    o = s
                } catch (e) {
                    n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
                }
                o.open()._l = function () {
                    var e = this.createElement("script");
                    n && (this.domain = n), e.id = "js-iframe-async", e.src = "https://assets.zendesk.com/embeddable_framework/main.js", this.t = +new Date, this.zendeskHost = "dsyh.zendesk.com", this.zEQueue = a, this.body.appendChild(e)
                }, o.write('<body onload="document._l();">'), o.close()
            }();
            /*]]>*/
        </script>

        <!--kissmetrics-->
        <script type="text/javascript">
            var kmq = kmq || [];
            var kmk = kmk || 'bbe550229413b1ca3a5c06bc3e010129e0e4ea11';
            function _kms(u) {
                setTimeout(function () {
                    var d = document, f = d.getElementsByTagName('script')[0],
                            s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = u;
                    f.parentNode.insertBefore(s, f);
                }, 1);
            }
            _kms('//i.kissmetrics.com/i.js');
            kms('//scripts.kissmetrics.com/' + kmk + '.2.js');
        </script>



        <!-- CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
        <link href="assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
        <link href="assets/css/animate.css" rel="stylesheet">
        <!-- Custom styles CSS -->
        <link href="assets/css/style.css" rel="stylesheet" media="screen">
        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <script>
            function alphanumeric(inputtxt) {
                var letters = /^[0-9a-zA-Z]+$/;
                if (inputtxt.value.match(letters))
                {
                    document.getElementById('error').style.display = "none";
                    return true;
                } else
                {
                    // hide body div tag
                    $('#error').show();
                    return false;
                }
            }
        </script>

        <style>
            #allerrors {
                /* height: 8px; */
                width: 50%;
                margin: auto auto;
                border-bottom: 0px solid #ff1f1f;
                display: none;
                color: #ffffff;
                font-weight: bold;
                transition: 0.6s;
                background: #ff5252;
                /* float: left; */
                text-align: -webkit-center;
                /* border-radius: 0% 0% 100% 100%; */
                /* height: 38px; */
                padding: 4px;
                box-shadow: 0px 2px 7px #404040;
            }

            #error {
                /* height: 8px; */
                width: 100%;
                margin: auto auto;
                border-top: 1px solid red;
                display: none;
                color: red;
                transition: 1s;
                float: left;
                /* position: absolute; */
                font-size: 12px;
            }


            .first {
                background: rgba(255, 253, 253, 0.09) no-repeat center;
                color: white;
                font-size: 123%;
                border: 0;
                /* padding: 20px 27px 10px 29px; */
                color: white;
                transition: 0.5s;
                width: 100%;
                height: 100%;
                outline: 0;
            }

            .second {
                outline: 0;
                background: #4CAF50 url(/img/mytick2.png) no-repeat 97% 87%;
                background-size: 25%;
                color: #ffffff;
                width: 100%;
                text-shadow: -1px -1px 0px rgba(0, 0, 0, 0.42);
                margin: 0;
                /* height: 141%; */
                font-size: 158%;
                box-shadow: inset 0px 16px 0px 0px rgba(255, 255, 255, 0);
                transition: 0.5s;
                /* text-align: -webkit-left; */
                border: 0;
                font-weight: 500;
                /* padding: 21px 29px 10px 29px; */
                /* font-size: 25px; */
                box-shadow: inset 0px 0px 0px 2px rgba(255, 255, 255, 0.42);
            }


            table{
                text-align: center;
            }


            td {
                border: 0;
                border-radius: 0px;
                border-bottom: 1px solid #4e4e4e;
                border-right: 1px solid #615f5f;
            }


            .snowwhite {
                background: #e6e6e6;
                color: black;
            }


            .white {
                background: white;
                color: grey;
            }


            .red {
                color: #F44336;

            }

            .violet {
                background: #673AB7;
                color: white;
            }


            .darkblue {
                background: #3F51B5;
                color: white;
            }


            .blue {
                background: #2196F3;
                color: white;
            }


            .green {
                background: #4CAF50;
                color: white;
            }


            .mango {
                background: #FF9800;
                color: white;
            }


            .orange {
                background: #FF5722;
                color: white;
            }

            .grey {
                background: #9E9E9E;
                color: white;
            }

            .brown {
                background: #795548;
                color: white;
            }
/*            .xmas{
                background: url(https://iconshow.me/media/images/xmas/christmas-icon4/2/christmas-tree-512.png) no-repeat 0% 122%;
                font-weight: 800;
                background-size: 37%;
                color: #ffffff ;
            }*/

            body {
                font-family: Open Sans!important;
            }

            nav {
                height: 50px;
                text-align: center;
                background: transparent;
                line-height: 70px;
                color: black;
                font-size: 40px;
            }

        </style>


        <style>
            /*
                        tr:nth-child(even) {
                background-color: #dddddd;
            }*/


            #c_name {
                background: url('img/c_name.png') no-repeat 13px;
                background-size: 20px;
                padding: 10px 45px;
                font-size: 23px;
                border-radius: 0px;
                padding: 0px 50px;
                /*box-shadow: inset -516px 0px 0px -2px white;*/
                border: 0;
                border-bottom: 1px solid #d0d0d0;
            }

            #c_name:focus{

                border: 0;
                border-bottom: 1px solid #868383;
                /* background: black; */
            }



            #c_aliasname {
                /*background: url(https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Simpleicons_Inter…face-symbol.svg/2000px-Simpleicons_Interface_link-interface-symbol.svg.png) no-repeat 14px;*/
                background-size: 17px;
                padding: 13px 45px;
                /* border: 0; */
                font-size: 23px;
                border-radius: 0px;
                padding: 0px 50px;
                /* box-shadow: inset -516px 0px 0px -2px white; */
                border: 0;
                border-bottom: 1px solid #d0d0d0;
            }

            #c_aliasname:focus{
                border: 0;
                border-bottom: 1px solid #868383;
                /* background: black; */
            }

            #c_email {
                background: white url(img/c_email.jpg) no-repeat 12px;
                background-size: 21px;
                font-size: 23px;
                padding: 0px 45px;
                /* font-size: 2vh; */
                border-radius: 0px;
                padding: 0px 50px;
                /*box-shadow: inset -516px 0px 0px -2px white;*/
                border: 0;
                border-bottom: 1px solid #d0d0d0;
            }

            .form-control:focus {
                font-size: 15px!important;
            }

            #c_email:focus{
                border: 0;
                border-bottom: 1px solid #868383;
            }

            #c_mob{
                background: #ffffff url(img/icon-mobile.png) no-repeat 10px;
                background-size: 25px;
                font-size: 23px;
                border-radius: 0px;
                padding: 0px 50px;
                /*box-shadow: inset -516px 0px 0px -2px white;*/
                border: 0;
                border-bottom: 1px solid #d0d0d0;
            }

            #c_mob:focus{
                border: 0;
                border-bottom: 1px solid #868383;
            }

            button.btn.btn-lg.btn-block.wow.fadeInUp.animated.animated:focus {
                background: green;
                color:white;
            }

            li {
                list-style-type: circle;
            }

            .header-m {
                font-size: 1.6em;
                font-weight: 600;
                color: #2196F3;
                text-align: left;
            }

            .Text-justify {
                text-align: justify;
            }

            .myquote > li {
                text-align: left;
                list-style-type: disc;
            }

            .modal-body {
                position: relative;
                padding: 15px;
                height: 77vh;
                overflow-y: auto;
            }

            .tooltip {
                font-size: 15px;
                font-weight: 100!important;
                padding: 0;
                margin: 0;
                color: lightgreen!important;
                /*border: firebrick;*/
            }

            .tooltip-inner{
                background: black;
                opacity: 1;
                background-color: teal!important;
            }

            .form-control {
                background: #FFF;
                border: 0;
                border-bottom: 1px solid #d0d0d0;
                font-size: 12px;
                padding: 0 15px;
                border-radius: 0;
            }

            select#statecode:hover {
                font-size: 15px!important;
            }

            .text-li > li {
                list-style-type: decimal;
                text-align: left;
                color: #ffffff;
                font-weight: 400;
            }
        </style>

<style>




@media only screen and (max-width: 1024px) and (max-height: 800px){

.smallTag {
    width: 100%!important;
    /* margin: 0!important; */
    font-size: 22px!important;
}
}

@media only  screen  and (max-width: 414px){
    .smallTag {
    width: 100%!important;
    /* margin: 0!important; */
    font-size: 12px!important;
}

.smallTag > big {
    font-size: 17px!important;
}

} 

@media only  screen  and (max-width: 375px){
    .smallTag {
    width: 100%!important;
    /* margin: 0!important; */
    font-size: 12px!important;
}

.smallTag > big {
    font-size: 15px!important;
}

} 



@media only  screen  and (width: 768px){
    .smallTag {
    width: 100%!important;
    /* margin: 0!important; */
    font-size: 12px!important;
}

.smallTag > big {
    font-size: 20px!important;
}

.ajax-hidden > table {
    width: 100%!important;
}

} 




</style>

    </head>




    <body>


        <div id="toastem"> </div>



        <!-- Navigation start -->

        <div id="TRUSTEDCOMPANY_widget_103142"><script async
            src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJXRF1XRFRSW0dZUUU"></script></div>


        <header class="header">
            <nav class="navbar navbar-custom" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php"><span>DSYH <img src="assets/images/dsyh_trans.gif" style="height:100%;" alt=""></span></a>
                    </div>
                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php#home">Home</a></li>
                            <li><a href="index.php#services">About Us</a></li>
                            <li><a href="index.php#portfolio">Insights</a></li>
                            <li><a href="index.php#skills">Savings Calculator</a></li>
                            <li><a href="index.php#teammembers">Team</a></li>
                            <li><a href="index.php#news">IN THE NEWS</a></li>
                        </ul>
                    </div>
                </div><!-- .container -->
            </nav>
        </header>
        <!-- Navigation end -->
    <center>
        <div>
            <div style="padding: 3vh 10vw;background: #4CAF50;background: linear-gradient(to right, #f07d3a , #02c0ee);color: white;text-align: -webkit-center;text-align: center;/* line-height: 24px; */height: 349px;font-size: 3vh;box-shadow: inset 0px 3px 0px #8BC34A;">

                <!-- <div style="
                     color: #ffffff;
                     text-shadow: 0px 1px grey;
                     display: block;
                     font-size: 45px;
                     font-weight: 700;
                     ">DSYH's REFUND GUARANTEE*</div>-->
                <div>
                    <small class="smallTag" style="
                           width: 69%;
                           display: block;
                           margin: auto;
                           margin-top: 12px;
                           font-weight: 600;
                           font-size: 17px;
                           ">
                        <!-- We guarantee you will receive the same amount in reimbursements from Amazon within your First 60 Days of membership or we will refund the cost of your membership as per your plan.(and you can keep what amazon reimbursed you!) <br> -->
                        <big style="
    /* text-shadow: 0px 1px 13px rgba(0, 0, 0, 0.19); */
    font-size: 22px;
    font-weight: 100;
    text-align: justify;
    "> If you a Seller on Amazon, you would know Reconciliation at Order ID level is very complex.
                                 We help you to reconcile your seller account and give you reports of <br><span class="type-content font-fam" style="color: #ffffff;font-weight: 800;text-shadow: -1px 1px grey;"><!--Modify in main.js --></span>
                                <!-- <ul class="text-li">
                                    <li>Wrong Deductions</li>
                                    <li>Missing Inventory Non Reimbursed </li> 
                                    <li>Overcharges </li>
                                    <li>Unsellable Inventory Non Reimbursement </li>
                                    <li>Replacement Reimbursements </li>
                                </ul>  -->
                        </big>
                        <br>
                        You can choose any of the plan for the Past Analysis for a particular period. If you wish our Expert Specialist raise cases and conduct follow ups for your Disputes, You can choose to ask our representative for the same.
                        </small></div>

            </div>
            <div style="
                 align-self: center;
                 width: 100%;
                 text-align: -webkit-center;
                 margin-left: 0%;
                 overflow-x:auto;
                 margin-top: -10.7vh;
                 " >

                <div>
                    <form name="myForm" action="register_new.php" id="registerationform"  role="form" method="POST">

                        <div class="ajax-hidden" style="/*overflow: auto;
                             overflow: -moz-hidden-unscrollable;
                             overflow: hidden;*/
                             " >

                            <!--                            <div style="
                                                             margin-left: 42%;
                                                             color: black;
                                                             width: 34vw;
                                                             ">* Service tax applicable extra</div>-->
                            <script>
$(document).ready(function () {
$('[data-toggle="tooltip"]').tooltip();
});
                            </script>
                            <?php
                            include 'db_conn_authval.php';
                            include 'newa.func.php';

                            $packagesq = good_query_table('select planname,details from packages where price<>0 and planstatus=1 group by planname order by serial_order asc');

                            if (!empty($packagesq)) {

                                echo '<table style="border: 0;margin: 0;padding: 0px;/* box-shadow: 0px 25px 0px 13px rgba(0, 0, 0, 0.18); */font-size: 3vmin;font-size: 3.2vh;width: 49%;font-size: 3.2vh;    height: 9.9pc;margin-top: 1vh;box-shadow: 0px 42px 38px -30px rgba(0, 0, 0, 0.26);"><tbody style="/* background: #3a3737; */color: white;font-family: ubuntu,segoe ui,segeo ui,roboto,helvetica;">';

                                echo '<tr class="black" style="font-weight: 100!important;font-size: 3vmin;background: transparent!important;">
                                	<td style="    border: 0;    font-size: 11px;    vertical-align: bottom;">* GST @ 18% extra</td>
                                    <td style="    border: 0;"></td>
                                    <td style="    border: 0;"></td>
                                    <td style="background: #000000;color: white;text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.19);border: 0px solid #2a2c2d;border-radius: 11px 11px 0px 0px; box-shadow: 0px -9px 9px -6px rgba(0, 0, 0, 0.15); font-size: 2.5vh;background: linear-gradient(#a0e254,#4caf50);background: linear-gradient(#FFC107
,#FF9800);/* background-size: 32px; *//* filter: hue-rotate(29deg); */">
                                    Best Seller</td><td style="border: 0;"></td></tr>';

                                echo '<tr class="black" style="font-weight: 400;font-size: 3vmin;background: rgb(42, 44, 45);" >	<td style="font-size: 4vh;color: #f2f9ff;text-shadow: 1px 1px black;text-align: -webkit-center;font-weight: 600;/* box-shadow: 5px -1px 4px 0px #000000; *//* position: absolute; */padding: 0px 11px;height: 50px;background: black;" >PLAN</td>';

//                echo '<!--<td>30 Days</td>--><td>3 Months</td><td style="background-size: 32px;filter: brightness(148%) hue-rotate();">6 Months</td><td>1 Year</td>';
                                echo '<td >3 Months</td><td >6 Months</td><td >12 Months</td> <td >Lifetime</td>';
                                echo '</tr>';


                                foreach ($packagesq as $p) {

                                    $planname = $p['planname'];
                                    $plandetails = $p['details'];

                                    echo '<tr class="colorname" style="font-size: 2vh;height: 100%;background: #3a3737;"><td style="padding:21px 6px; line-height:17px;">' . $planname . '<br><span style="font-size: 1.5vh;    padding: 1vh;    width: 2vh;    color: rgb(135, 225, 146);    ">(' . $plandetails . ')</span></td>';

                                    $qgetprices = good_query_table('select * from packages where planname="' . $p['planname'] . '" and price<>0 and planstatus=1 group by period order by price asc');

                                    if (!empty($qgetprices)) {
                                        $arrsize = 3;
                                        $sz = 1;
                                        $colspanval = '';
                                        if (sizeof($qgetprices) == '2') {
                                            $colspanval = "colspan=2";
                                        }

//            echo "<PRE>";
//            print_r(sizeof($qgetprices));

                                        foreach ($qgetprices as $qp) {
                                            $qprice = $qp['price'];
                                            echo '<td ' . $colspanval . '><input  data-toggle="tooltip"   data-placement="bottom"  id="' . $qp['id'] . '"  type="button"   value="₹ ' . $qprice . '" class="first" data-tid="' . $qp['id'] . '" style="border:0;float:left;height: 100%;" > </td>';
                                            $sz++;
                                        }


//            continue;
                                        if ($colspanval != "")
                                            continue;



                                        if ($sz <= $arrsize) {
                                            for ($a = $sz; $a <= $arrsize; $a++) {
                                                echo '<td> <input type="button" disable style="
                    /* background: grey; */
                    font-size: 14px;
                    /* font-style: italic; */
                    background: rgb(76, 73, 73) no-repeat center;
                    /* color: white; */
                    /* font-size: 23px; */
                    border: 0;
                    /* padding: 20px 27px 10px 29px; */
                    color: #a79f9f;
                    transition: 0.5s;
                    width: 100%;
                    height: 100%;
                    outline: 0;
                    text-align: center;
                    height: 100%;
                    " value="coming soon"/></td>';
                                            }
                                        }
                                    }

                                    echo '</tr>';
                                }
                                echo '</tbody></table>';
                            }
                            ?>

                            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

                        </div>

                        <div id="allerrors">
                            <span>Select Your PLAN!!</span>
                        </div>

                        <!-- Contact start -->

                        <section id="contact" class="pfblock">

                            <div class="container">

                                <div class="row">

                                    <div class="col-sm-6 col-sm-offset-3" style="background: white;
                                         /*box-shadow: 0px 8px 19px rgba(128, 128, 128, 0.26);*/
                                         padding-top: 22px;padding-bottom: 14px;
                                         /*border: 2px solid rgb(208, 208, 208);*/
                                         ">

                                        <input type="hidden"  id="ptid" class="form-control" name="ptid" placeholder="SELECT A PLAN" value="" required style="color: transparent;border: medium none transparent;">

                                        <div class="form-group wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                                            <label class="sr-only" for="c_name"><span><strong> <label for="c_name">Company Name</label></strong> <span class="red">(must be correct)</span></pre></label>
                                            <input type="text" pattern="^[0-9a-zA-Z-,]+(\s{0,1}[a-zA-Z-,&! ])*$" required id="c_name" autocomplete="OFF" class="form-control" onkeypress="cnamecheck();" name="c_name" placeholder="Company Name" >
                                        </div>

                                        <div class="form-group wow fadeInUp animated animated" style="visibility: visible;/* height: 34px; */animation-name: fadeInUp;width: 100%;display: flex;" align="center">
                                            <label class="sr-only" for="c_aliasname"><span style=" "><strong><label for="c_aliasname">ALIAS NAME</label></strong> (will be your subdomain)  <span class="red"><i>[special chars not allowed]</i></span></pre></label>
                                            <input type="text" class="form-control" name="c_aliasname" style="/* -webkit-appearance: none!important; */color: #404040;text-align: right;width: 9%;border-bottom: 0px solid rgba(0, 150, 136, 0.45);background: #ffffff url(assets/images/dsyh_trans.gif) no-repeat 47% 50%;/* float: left; *//* filter: grayscale(); */background-size: 58%;/* border-left:0px; */padding-right: 1px;margin: 0;/* background:white; */font-size: 23px!important;/* -webkit-appearance: none!important; */border:0px solid gray;border-right:0px;cursor: pointer;/* outline:none; */padding: 0;font-weight: 800;border-bottom: 1px solid #d0d0d0;border-radius: 0;" value=" " disabled="">

                                            <input type="text" pattern="^[a-zA-Z0-9]*$" onkeypress="return blockSpecialChar(event);" required="required" id="c_aliasname" autocomplete="OFF" data-validation="alphanumeric" class="form-control" name="c_aliasname" placeholder="aliasname" style="color: rgb(0, 150, 136); text-align: left; width: 79%; padding: 0px;">

                                            <input type="text" class="form-control" style="-webkit-appearance: none!important;color: #009688;text-align: right;width: 14%;border-bottom: 0px solid rgba(0, 150, 136, 0.45);/* float: left; *//* border-left:0px; */padding-right: 3px;margin: 0;background:white;font-size: 23px!important;/* -webkit-appearance: none!important; */border:0px solid gray;border-right:0px;outline:none;padding: 0;/* font-weight: 800; *//* background: #e2e2e2; */" value=".dsyh.in" disabled="">

                                            <div id="error">
                                                <span>please use only letters and numbers</span>
                                            </div>

                                            <!--<label for="c_aliasname" style="    display: flex;
                                                color: #b8b8b8;
                                                text-align: right;
                                                margin: 4px;
                                                position: absolute;
                                                right: -30%;
                                            " align="center">(Create your Subdomain)</label>-->
                                        </div>
                                        <div class="form-group wow fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                            <label class="sr-only" for="c_email"><span style=""><strong><label  for="c_email">Email</label></strong> (email@example.com) <span class="red"> must be valid!</span></pre></label>
                                            <input type="text" required="required"  pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  id="c_email" class="form-control" autocomplete="OFF" name="c_email" placeholder="E-mail" >
                                        </div>

                                        <div class="form-group wow fadeInUp animated animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                            <label class="sr-only" for="c_mob">
                                                <span style="">
                                                    <strong>
                                                        <label for="c_mob">Mobile number</label></strong>
                                                    <span class="red"> must be valid!</span>
                                                </span>
                                            </label>
                                            <input type="text"  required="required" pattern=".{9,10}" id="c_mob"   maxlength="10" minlength="10" size="10" class="form-control" onkeypress="return isNumber(event)" autocomplete="OFF" name="c_mob" placeholder="Mobile Number" >
                                        </div>


                                        <div class="form-group wow fadeInUp animated animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                            <label class="sr-only" for="statecode">

                                                <span style="">
                                                    <strong>
                                                        <label for="statecode">State</label>
                                                    </strong>
                                                    <span class="red"> is not selected</span>

                                            </label>
                                            <!--<input type="text"  required="required" pattern=".{9,10}" id="c_mob"   maxlength="10" minlength="10" size="10" class="form-control" onkeypress="return isNumber(event)" autocomplete="OFF" name="c_mob" placeholder="Mobile Number" >-->
<select id="statecode" name="statecode" required="required" class="form-control" style="background: url(/img/icon-state.png) no-repeat;background-size: 18px;background-position: 13px 7px;padding-left: 44px;font-size: 23px;color: #a0a0a0;">
<option value=""  > Select State</option>
        <?php
        $statedrop = '{"1":"Jammu and Kashmir","2":"Himachal Pradesh","3":"Punjab","4":"Chandigarh","5":"Uttarakhand","6":"Haryana","7":"Delhi","8":"Rajasthan","9":"Uttar Pradesh","10":"Bihar","11":"Sikkim","12":"Arunachal Pradesh","13":"Nagaland","14":"Manipur","15":"Mizoram","16":"Tripura","17":"Meghalaya","18":"Assam","19":"West Bengal","20":"Jharkhand","21":"Orissa","22":"Chattisgarh","23":"Madhya Pradesh","24":"Gujarat","25":"Daman and Diu","26":"Dadra and Nagar Haveli","27":"Maharashtra","28":"Andhra Pradesh","29":"Karnataka","30":"Goa","31":"Lakshadweep Islands","32":"Kerala","33":"Tamil Nadu","34":"Pondicherry","35":"Andaman and Nicobar Islands","36":"Telangana"}';
        $jsondecode = json_decode($statedrop, true);
        foreach ($jsondecode as $stcode => $st) {echo('<option value='.$stcode.'>'.$st.'</option>');}
        ?>
</select>

                                        </div>

                                        <!--                    <div class="form-group wow fadeInUp animated animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                                              <label class="sr-only" for="promocode">Promo Code</label>
                                                              <input type="text" id="promocode"   maxlength="10" size="10" class="form-control"  autocomplete="OFF" name="promocode" placeholder="Promo Code" style="
                                                                     border: 1px solid #e4e4e4 !important;
                                                                     ">
                                                            </div>-->


                                        <div class="form-group wow fadeInUp animated animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                            <input type="checkbox" id="agreed" required="required"   class="form"  autocomplete="OFF" name="agreed"  >
                                            <small class="" for="agreed" >i agree to the <a href="#" data-toggle="modal" data-target="#myModal">terms and conditions</a></label>
                                                <label class="sr-only" for="agreed">
                                                    <span style="">
                                                        <strong><label  for="agreed"> Please Agree the Terms & Conditions</label></strong>  click on the checkbox <span class="red"> </span></pre></label>

                                        </div>
                                        <ul class="errorMessages" style="list-style: none;position: absolute;right: -84%;top: 0%;text-align: left;background: #ffefef;box-shadow: #ff7070 0px 1px 7px -1px;"></ul>
                                        <script>
                                            function isNumber(evt) {
                                                evt = (evt) ? evt : window.event;
                                                var charCode = (evt.which) ? evt.which : evt.keyCode;
                                                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                                                    return false;
                                                }
                                                return true;
                                            }
                                        </script>


                                        <button type="submit" id="revenuebtn" target="_blank"  class="btn btn-lg btn-block wow fadeInUp animated animated" style="visibility: visible;animation-name: fadeInUp;font-size: 17px;/* background: #222222; */font-weight: 600;">
                                            <small style="color: #89d92c;">Increase Your Revenue</small>
                                            <br>
                                            <big>GET STARTED NOW</big>
                                        </button>

                                    </div>
                                    <div class="ajax-response"></div>
                                </div>

                            </div><!-- .row -->
                        </section>
                    </form>
                    <!-- Contact end -->
<br>
                    <!-- Footer start -->
                    <div id="help" style="display:none!important;padding: 7px;font-weight: 600;color: rgb(148, 148, 148);width: 32%;left: 2%;margin: 12px;border-radius: 4px;">

                        <span style="color: #f38880;float: left;font-size: 25px;font-weight: 300;width: 100%;">Not Sure how to move ahead? </span>
                        <span style="
                              font-size: 17px;
                              font-weight: 100;
                              color: #607D8B;
                              ">  Call our Reconciliation Expert </span><span style="
                              color: #ffffff;
                              text-shadow: 0px 1px 1px grey;
                              font-family: sans-serif;
                              font-size: 1.7vh;
                              font-weight: 600;
                              border-radius: 112px;
                              /* border: 1px solid  #4caf50; */
                              padding: 3px 7px;
                              width: 39%;
                              background: linear-gradient(to right, #4caf50 , #8BC34A);
                              display: -webkit-box;
                              "><a href="tel:9081799777" style="
                             color: #ffffff;
                             ">+91-9081799777</a></span>

                        <span style="
                              color: #ffffff;
                              text-shadow: 0px 1px 1px grey;
                              font-family: sans-serif;
                              font-size: 1.4vh;
                              font-weight: 600;
                              border-radius: 112px;
                              padding: 3px 7px;
                              background: linear-gradient(to right, rgba(138, 195, 74, 0.8) , rgba(77, 175, 79, 0.7));
                              margin-top: 4px;
                              width: 39%;
                              display: grid;
                              ">
                            <a href="tel:+91-9723037000" style="
                               color: #ffffff;
                               "> +91-9723037000</a></span>

                    </div>
                    <div style="
                         padding: 23px;
                         background: linear-gradient(to right, #222222 , #f7b101, #222222);
                         color: white;
                         text-align: -webkit-center;
                         text-align: center;
                         line-height: 0px;
                         height: 48px;
                         font-size: 2vw;
                         font-size: 16px;
                         font-weight: 500;
                         ">Support Hours
                        Monday - Saturday
                        9am to 6pm<span style="font-weight: 800;color: #ffffff;margin-top: 0px;">   * * *   </span>
                        Critical Incidents are Monitored 24/7</div>

                    <footer id="footer">
                        <div class="container">
                            <div class="row">



                                <div class="trustedcompany-seal">
                                    <a href="https://trustedcompany.com/in/dsyh.in-reviews" target="_blank" title="Dsyh reviews on TrustedCompany.com">
                                        <img src="//trustedcompany.s3.amazonaws.com/sites/all/modules/custom/tc_site_integrations_seal/images/seal.png" alt="Dsyh reviews on TrustedCompany.com" style="width: 100px; height: 115px;" />
                                    </a>
                                </div>
                                <div id="TRUSTEDCOMPANY_widget_103142"><script async
                                    src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJXRF1XRFRSW0dZUUU"></script></div>

                                <br>
                                <div class="col-sm-12">

                                    <ul class="social-links">
                                        <li><a href="https://www.facebook.com/dontscratchyourhead/" target="_blank" class="wow fadeInDown"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/DSYH001" target="_blank" class="wow fadeInUp" data-wow-delay=".1s"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://plus.google.com/114063526780790560900" class="wow fadeInDown" target="_blank" data-wow-delay=".2s"><i class="fa fa-google-plus"></i></a></li>

                                        <li><a href="mailto:help@dsyh.in" class="wow fadeInDown"  target="_blank" data-wow-delay=".5s"><i class="fa fa-envelope"></i></a></li>
                                        <li><a href=" https://www.linkedin.com/company/dsyh" target="_blank" class="wow fadeInDown" data-wow-delay=".6s"><i class="fa fa-linkedin"></i></a></li>


                                    </ul>

                                    <p class="heart">
                                        Made with <span class="fa fa-heart fa-2x animated pulse"></span> in India
                                    </p>
                                    <p class="copyright">
                                        © 2015 DSYH
                                    </p>

                                </div>

                            </div><!-- .row -->
                        </div><!-- .container -->
                    </footer>





                    <!-- Footer end -->

                    <!-- Scroll to top -->

                    <div class="scroll-up">
                        <a href="#home"><i class="fa fa-angle-up"></i></a>
                    </div>

                    <!-- Scroll to top end-->

                    <!-- Javascript files -->

                    <script src="assets/js/jquery-1.11.1.min.js"></script>
                    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
                    <script src="assets/js/jquery.parallax-1.1.3.js"></script>
                    <script src="assets/js/imagesloaded.pkgd.js"></script>
                    <script src="assets/js/jquery.sticky.js"></script>
                    <script src="assets/js/smoothscroll.js"></script>
                    <script src="assets/js/wow.min.js"></script>
                    <script src="assets/js/jquery.easypiechart.js"></script>
                    <script src="assets/js/waypoints.min.js"></script>
                    <script src="assets/js/jquery.cbpQTRotator.js"></script>
                    <script src="assets/js/custom.js"></script>





                    <!-- Modal for terms & conditions -->

                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Agreement</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="header-m">Terms & Conditions</div>
                                    <p class="Text-justify">
                                        This Terms of Use document (“<strong>TOS</strong>”) is a legal agreement between you, the end user (“<strong>You</strong>”/“<strong>Your</strong>”), and Recconext Labs Private Limited (“DSYH”), the owner of all intellectual property rights in the cloud-based business inventory platform titled “<strong>DSYH</strong>” (“<strong>Platform</strong>”). The TOS describes the terms on which DSYH offers access to the Platform and the Services (defined below) to all end users.
                                    <p class="Text-justify"><strong> PLEASE READ THE TOS CAREFULLY. YOUR ACCEPTANCE OF THIS TOS AND USE OF THE PLATFORM SHALL SIGNIFY YOUR ACCEPTANCE OF THE TOS, THE PRIVACY POLICY AVAILABLE AT www.dsyh.in/privacy-policy, AND THE OTHER WEBSITE POLICIES AS WELL AS YOUR AGREEMENT TO BE LEGALLY BOUND BY THE SAME.</strong></p><p  class="Text-justify">
                                        This is a legally binding agreement and is enforceable against you. DSYH may modify the terms of the TOS at any time and shall inform you of the same. You will be deemed to have accepted the revised TOS if You continue to access the Platform after the modifications become effective. If You do not agree to the terms of this TOS, please do not access or use the Platform.
                                        The following are the terms and conditions on which DSYH agrees to permit you to access and use the Platform and the Services.
                                    </p>
                                    <div class="header-m">1. REGISTRATION</div>
                                    <p class="Text-justify">
                                    <ul class="myquote"><li>In order to access and use the Platform, You will be required to register yourself and maintain a DSYH account (“<strong>Account</strong>”) which will require you to furnish to DSYH, certain information and details, including your name, e-mail id, and any other information deemed necessary by DSYH (“<strong>Account Information</strong>”). You agree to keep this information updated at all times.</li><li>
                                            You shall be responsible for maintaining the confidentiality and security of the password and for all activities that occur in and through Your Account. DSYH and its affiliates / partners are not liable for any harm caused by, or related to the theft of, Your ID, Your disclosure of Your Account Information, or Your authorization to allow another person to access and use the Service using Your Account. However, you may be liable to DSYH and its affiliates / partners for the losses caused to them due to such unauthorized use.</li>
                                        <li><strong>Account Dos and Don’ts</strong>: You agree to abide by the following dos and don’ts for registering and maintaining the security of Your Account.</li>
                                        <li>You will create the Account on behalf of an entity or firm or in Your individual capacity only if You are a natural person aged 18 years or above and competent to enter into a valid and binding contract.</li>
                                        <ul class="myquote"><li>
                                                You will not provide any false personal information to DSYH.</li>
                                            <li>You shall ensure that the Account Information is complete, accurate and up-to-date at all times.</li>
                                            <li>You shall not use any other user’s Account Information or log into that user’s Account.</li>
                                            <li>You will not share Your password or do anything that might jeopardize the security of Your Account.</li>
                                            <li>On completing the registration process, You shall be entitled to access the Platform and avail of the Services.</li>
                                            <li>Your account, ID, and password may not be transferred or sold to another party.</li>
                                            <li>You agree to immediately notify DSYH of any unauthorized use of Your account or any other breach of security known to You.</li>
                                            <li>In order to ensure that DSYH is able to provide high quality services, respond to customer needs, and comply with laws, You hereby consent to let DSYH’s employees and agents access Your Account and records on a case-to-case basis to investigate complaints or other allegations or suspected abuse. Additionally, You agree to disclose to DSYH, and permit DSYH to use, Your log-in ID, password and such other account details with respect to Your account(s) with e-commerce websites/platforms, for the limited purpose of resolving technical problems.</li>
                                            <li>You also grant DSYH the right to disclose to its affiliates / partners / third parties Your Account Information to the extent necessary for the purpose of rendering the Services.
                                            </li></ul></ul>
                                    </p>
                                    <div class="header-m"> 2. SCOPE OF SERVICES </div>
                                    <p class="Text-justify">
                                    <ul class="myquote"><li>DSYH hereby grants You a specific, non-exclusive, non-transferrable and limited license to access and use the Platform (which is public cloud hosted order and/or inventory management application) via the internet, and avail of the services provided by the Platform with respect to warehouse management (“Services”). DSYH reserves the right to make changes to the functionality or the documentation of the Platform and the provision of Services from time to time.</li>
                                        <li>
                                            DSYH retains all rights in the Platform and the Services, and grants You only a right and license to use the Platform / application as stated herein. No other license is intended to be granted to You. DSYH reserves all rights in its name, trademarks, copyrights and any other intellectual property.</li>
                                        <li>
                                            You may use the Platform and the Service to store data, print and display data in the Service. No other use of the Platform and the Service by You shall be permitted.</li>
                                        <li>
                                            You may access the Platform and the Services using a single user Account via multiple access points. You may allow Your employees, agents and independent contractors to access the Platform via Your Account on Your behalf. However, You agree not to provide any access to Your Account to any third party vendors.</li>
                                        <li>
                                            DSYH shall perform all necessary server management and maintenance services with respect to the Platform at no additional cost to You.</li><li>
                                            DSYH does not guarantee availability of the Platform at all times. DSYH shall use reasonable efforts to make the Services available to You, at all times through the Platform. However, as the Services are provided over the Internet, data and cellular networks, the quality and availability of the same may be affected by factors outside DSYH’s control. Therefore, DSYH shall not be liable for non-availability of the Services at any time. DSYH may try and restore access to the Platform and the Services on a best reasonable and commercially viable basis.</li></ul></p>
                                    <div class="header-m"> 3. USE OF SERVICES </div>
                                    <p class="Text-justify">
                                    <ul class="myquote"><li>
                                            The Platform is a standard off-the-shelf application. We do not provide any customization in the platform.</li>
                                        <li>
                                            You agree to use the Services solely for the purpose for which the Services are provided, namely warehousing, and solely to aid Your business. You shall not sublicense or resell the Platform or the Services for the use or benefit of any other organization, entity, business or enterprise.</li>
                                        <li>
                                            You agree not to submit or upload to the Platform, any material that is illegal, misleading, defamatory, indecent or obscene, threatening, infringing of any third party proprietary rights, invasive of personal privacy or otherwise objectionable (collectively, “<strong>Objectionable Matter</strong>”). DSYH reserves the right to adopt and amend rules for the permissible use of the Platform and the Services at any time, and You shall be required to comply with such rules. You shall also be required to comply with all applicable laws regarding privacy, data storage etc., or any other policy of DSYH, as updated from time to time. DSYH reserves the right to terminate this Agreement and Your access to the Platform, without notice, if You commit any breach of this clause.</li><li>
                                            As part of the Services, the Platform allows You to upload data / content to it. All user data uploaded or submitted by You to Your Account, shall be Your sole property. You retain all rights in the data uploaded by You to the Platform and shall remain liable for the legality, reliability, integrity, accuracy and copyright permissions thereto of such data. DSYH will use commercially reasonable security measures to protect the User’s data against unauthorized disclosure or use. However, DSYH does not guarantee data security. If Your data is damaged or lost, DSYH will use commercially reasonable means to recover such data. You agree that You are entering into this agreement in full knowledge of the same.</li>
                                        <li>
                                            You shall not alter, resell or sublicense the Platform or the Services to any third party. You shall not reverse engineer the Platform or its software or other technology, circumvent, disable or otherwise interfere with security-related features or any digital rights management mechanisms of the Platform. You will not use the Platform or the Service to (i) build a competitive product or service, (ii) make or have a product with similar ideas, features, functions or graphics of the Platform, (iii) make derivative works based on the Platform / Services; or (iv) copy any features, functions or graphics of the Platform/Services.</li>
                                        <li>
                                            DSYH is an intermediary as defined under the Information Technology Act, 2000. DSYH does not monitor or control any data or content uploaded by You to the Platform. You agree not to use or encourage, or permit others to store, upload, modify, update or share any information that:</li>
                                        <ul  class="myquote">
                                            <li>belongs to another person and to which you do not have any right;</li>
                                            <li>is grossly harmful, misleading, harassing, blasphemous defamatory, indecent, obscene, pornographic, paedophilic, libellous, invasive of another’s privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, invasive of personal privacy or otherwise objectionable or any data / content that is contrary to any applicable local, national, and international laws and regulations;</li>
                                            <li>infringes any patent, trademark, copyright or other proprietary rights;</li>
                                            <li>violates any law for the time being in force;</li>
                                            <li>results in impersonation of any person or entity, or falsely states or otherwise misrepresents your affiliation with a person or entity;</li>
                                            <li>is someone’s identification documents or sensitive financial information;</li>
                                            <li>contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;</li>
                                            <li>threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognisable offence or prevents investigation of any offence or is insulting any other nation; or</li>
                                            <li>makes available any data / content in contravention of these TOS or applicable policies, or any data / content that You do not have a right to access, store, use or make available to third parties under any law or contractual or fiduciary relationship.</li></ul>
                                        <li>2 DSYH reserves the right to suspend or terminate Your access to Your Account if You cause any disruption or harm to the DSYH infrastructure or to any third parties, or violate the provisions of the Information Technology Act, 2000, any applicable privacy laws or any of the applicable laws. You hereby consent to let DSYH’s employees and agents access Your Account and records on a case-to-case basis to investigate complaints or other allegations or suspected abuse.</li>
                                        <li>3 The free trial version of the software will be available only for 3 months from the date of sign up.</li></ul>
                                    </p>
                                    <div class="header-m">4. FEES</div>
                                    <p class="Text-justify">
                                    <ul  class="myquote">
                                        <li>This is a paid version of the Platform. The Platform works on a prepaid or recharge model where You may choose from the paid or recharge options (“Fees”) by going to the billing section of your DSYH account.</li>
                                        <li>The Fees shall be exclusive of all applicable taxes. You may choose to pay the Fees by any of the payment options made available by DSYH including, credit card, debit card, net banking. There will be no deduction of TDS in case of use of any of the online methods for payments of Fees. However, if You choose to pay by cheque and deduct TDS on the Fees, You shall be given credit for the amount of TDS deducted. A processing fee of Rs 200/- will be charged to You in case of cheque payments. If DSYH changes the Fees payable, DSYH shall give You advance notice of these changes via a message to the email address associated with Your Account. DSYH will bill You through Your chosen payment method, from the date You opt for the paid Services option until termination. All payments are final and non-refundable. You will not be entitled to any cancellation or cooling off period after opting for the paid Services.</li>
                                        <li>All advance paid is non-refundable and initial upgrade amount paid will have validity of 30 days. If customer is unable to go live and start-processing orders in specified period, his account will be disabled and upgrade fees will be adjusted.</li>
                                        <li>Training and support is provided over phone and email only. You can opt for physical training by paying Rs 2500 per 4 hour training slot in advance to the company.</li>
                                        <li>The Services are provided to You via the internet and data and cellular networks, relevant internet charges and network or data charges, roaming charges, etc., applicable for Your use of the internet and the data shall apply (over and above the Fees) while accessing the Platform and availing the Services. You accept responsibility for all such charges that may arise due to Your use of the Platform and the Services.</li>
                                        <li>DSYH reserves the right to change the fees charged for any product type at any point of time without prior notice.</li>
                                        </p>

                                        <div class="header-m">5. INDEMNIFICATION</div>
                                        <p class="Text-justify">
                                            You shall indemnify and hold harmless, DSYH, its affiliates, any third party content / networks / infrastructure providers and their respective directors, officers, personnel, contractors and agents, for and against any and all claims, losses, damages, costs and expenses arising out of, or relating to, Your use of the Platform and the Services or Your breach of the TOS or any other restrictions or guidelines provided by DSYH. This indemnification obligation will survive at all times, including, Your use of the Platform and the Services.
                                        </p>


                                        <div class="header-m">6. DISCLAIMER OF WARRANTIES</div>
                                        <p class="Text-justify">
                                        <ul  class="myquote">
                                            <li>THE PLATFORM AND THE SERVICES ARE PROVIDED ON AN “AS-IS” AND “WITH ALL FAULTS AND RISKS” BASIS, WITHOUT WARRANTIES OF ANY KIND. DSYH DOES NOT WARRANT, EXPRESSLY OR BY IMPLICATION, THE ACCURACY OR RELIABILITY OF THE PLATFORM OR THE SERVICES OR ITS SUSTAINABILITY FOR A PARTICULAR PURPOSE OR THE SAFETY/SECURITY OF THE DATA/CONTENT STORED ON THE PLATFORM BY YOU. DSYH DISCLAIMS ALL WARRANTIES WHETHER EXPRESS OR IMPLIED, INCLUDING THOSE OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, OR THAT USE OF THE PLATFORM OR ANY MATERIAL THEREOF WILL BE UNINTERRUPTED OR ERROR-FREE. WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, DSYH DOES NOT REPRESENT OR WARRANT THAT THE PLATFORM AND THE SERVICES WILL RESULT IN COMPLIANCE, FULFILLMENT OR CONFORMITY WITH THE LAWS, REGULATIONS, REQUIREMENTS OR GUIDELINES OF ANY GOVERNMENT OR GOVERNMENTAL AGENCY.</li>
                                            <li>To the maximum extent permitted by applicable law, DSYH provides no warranty on the use of the Platform and the Services, and shall not be liable for the same under any laws applicable to intellectual property rights, libel, privacy, publicity, obscenity or other laws. DSYH also disclaims all liability with respect to the misuse, loss, modification or unavailability of the Platform and the Services.</li></ul>
                                        </p>

                                        <div class="header-m">7. LIMITATION OF LIABILITY</div>
                                        <p class="Text-justify">

                                        <ul  class="myquote">
                                            <li>YOU ASSUME THE ENTIRE RISK OF USING THE PLATFORM AND THE SERVICES. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL DSYH BE LIABLE TO YOU FOR ANY SPECIAL, INCIDENTAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF DATA OR INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF, OR INABILITY TO USE OR ACCESS THE PLATFORM OR THE SERVICES OR FOR ANY SECURITY BREACH OR ANY VIRUS, BUG, UNAUTHORIZED INTERVENTION, DEFECT, OR TECHNICAL MALFUNCTIONING OF THE PLATFORM, WHETHER OR NOT FORESEEABLE AND WHETHER OR NOT DSYH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR BASED ON ANY THEORY OF LIABILITY, INCLUDING BREACH OF CONTRACT OR WARRANTY, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR ANY OTHER CLAIM ARISING OUT OF, OR IN CONNECTION WITH, YOUR USE OF, OR ACCESS TO, THE SOFTWARE OR THE SERVICES. FURTHER, DSYH SHALL NOT BE LIABLE TO YOU FOR ANY TEMPORARY DISABLEMENT, PERMANENT DISCONTINUANCE OF THE SERVICES BY DSYH, DATA LOSS OR FOR ANY CONSEQUENCES RESULTING FROM SUCH ACTIONS.</li>
                                            <li>DSYH’S AGGREGATE LIABILITY, IF ANY, (WHETHER UNDER CONTRACT, TORT INCLUDING NEGLIGENCE, WARRANTY OR OTHERWISE) AND THAT OF ITS AFFILIATES SHALL BE LIMITED TO THE TOTAL AMOUNT OF FEES RECEIVED FROM YOU FOR THE ONE (1) MONTH IMMEDIATELY PRECEDING THE DATE THE CLAIM WAS MADE. DAMAGES, IN THE NATURE, AND TO THE AMOUNT, PROVIDED IN THIS CLAUSE, IS THE ONLY RECOURSE THAT YOU MAY HAVE AGAINST DSYH FOR BREACH BY DSYH OF ANY OF ITS RIGHTS OR OBLIGATIONS HEREUNDER.</li></ul>
                                        </p>
                                        <div class="header-m">8. NOTICE AND TAKEDOWN MECHANISM</div>
                                        <p class="Text-justify">
                                        <ul  class="myquote">
                                            <li>    The content uploaded by You may be available on the e-commerce websites / marketplaces in India or abroad, as chosen by You. In such circumstances, the notice and takedown provisions applicable to the particular e-commerce websites / marketplaces will be applicable to the content uploaded by You. Content which is objectionable or infringing will be accordingly taken down. For all other times, the notice and takedown mechanism provided below will apply.</li>
                                            <li>
                                                If You are the owner of copyright in any content shared or uploaded on the Platform without Your consent, or You believe that any user of the Platform is storing, hosting, uploading or transmitting data / content, then You are required to send a written notice to the Grievance Officer at DSYH providing the following information/details:</li>
                                            <ul  class="myquote">
                                                <li>
                                                    Description of the work with adequate information to identify the data / content;</li>
                                                <li>Details establishing that you are the owner or the exclusive licensee of copyright in the content;</li>
                                                <li>Details establishing that the copy of the content in question is an infringing copy of the content owned by You and that the allegedly infringing act is not considered as a ‘non-infringing’ act under Section 52 of the Copyright Act and is not any other act that is permitted under the Copyright Act;</li>
                                                <li>Details of the location where the content in question is stored (i.e., the URL of the page where such content is stored);</li>
                                                <li>Details of the person, if known, who has uploaded the infringing copy of the content; and</li>
                                                <li>Undertaking that You shall file an infringement suit in the competent court against such person uploading the infringing copy and provide a copy of the orders so obtained to DSYH within twenty one (21) days from the date of receipt of the notice by DSYH.</li></ul>
                                            <li>2 You may also inform the Grievance Officer / Nodal Officer if any content that is in violation of the TOS.</li>
                                            <li> The details of the Grievance Officer and the Nodal Officer are as provided below:</li>

                                            <div style="background:white;border: 1px solid grey;color: black;">
                                                <ul class="myquote">
                                                    <li> Name of the Grievance Officer: Prem Solanki</li>
                                                    <li> Address of the Grievance Officer: LL/1 President House, Ambawadi, Ahmedabad</li>
                                                    <li> Email Address: <a href="mail:help@dsyh.in" >help@dsyh.in</a></li>
                                                    <li> Contact Number: +91- 9099757928</li>
                                                </ul>
                                            </div>

                                            <div style="background:white;border: 1px solid grey;color: black;">
                                                <ul class="myquote">
                                                    <li>Name of the Nodal Officer: Sumit Karanji</li>
                                                    <li>Address of the Nodal Officer: LL/1 President House, Ambawadi, Ahmedabad</li>
                                                    <li>Email Address: <a href="mail:ssk@dsyh.in" >ssk@dsyh.in</a></li>
                                                    <li>Contact Number: +91-9099757928</li>
                                                </ul>
                                            </div>

                                            <li>
                                                3 If You knowingly misrepresent that any material or activity is infringing, You may be subject to legal liability. Accordingly, if You are not sure whether material available online infringes your copyright, please contact a lawyer.
                                            </li></ul>
                                        </p>

                                        <div class="header-m"> 9. SUSPENSION AND TERMINATION </div>
                                        <p class="Text-justify">
                                        <ul class="myquote">
                                            <li>
                                                The Services will remain in effect until suspended or terminated under this TOS.</li>
                                            <li>You may terminate Your registration by sending an email to <a href="mail:terminate@DSYH.com" >terminate@DSYH.com</a>, if You no longer wish to use the Platform. On termination, You will cease to have access to the Platform or any of the Services.</li>
                                            <li>
                                                DSYH reserves the right to suspend or terminate Your Account or restrict or prohibit You access to the Platform immediately (a) if DSYH is unable to verify or authenticate Your registration data, email address or other information provided by You, (b) if DSYH believes that Your actions may cause legal liability for You or for DSYH, or all or some of DSYH’s other users, or (c) if DSYH believes You have provided false or misleading registration data or other information, have not updated Your Account Information, have interfered with other users or the administration of the Services, or have violated this TOS or the Privacy Policy. You shall not be entitled to access the Platform or avail the Services if Your Account has been temporarily or indefinitely suspended or terminated by DSYH for any reason whatsoever. DSYH may, at any time, reinstate any suspended Account, without giving any reason for that. If You have been indefinitely suspended You shall not register or attempt to register with DSYH or its affiliates / partners or use the Services in any manner whatsoever until such time that You are reinstated by DSYH.</li>
                                            <li>In addition to the above, If you are not able to go live and start processing orders within 30 days of up gradation date, DSYH shall be entitled to terminate Your Account irrespective of the balance amount available in Your Account. In such cases, DSYH shall not be liable to refund any Fees to You.</li>
                                            <li>In addition to the above, if You do not process any order through the Platform for a continuous period of one (1) month, DSYH shall be entitled to terminate Your Account irrespective of the balance amount available in Your Account. In such cases, DSYH shall not be liable to refund any Fees to You.</li>
                                            <li> Upon termination of this TOS, Your right to access the Platform and use the Services shall immediately cease. Thereafter, You shall have no right, and DSYH shall have no obligation thereafter, to execute any of the uncompleted tasks.</li>
                                            <li>DSYH does not have a refund policy and therefore, no refund of the Fees shall be provided under any circumstance.</li>
                                            <li>Once the Services are terminated or suspended, any data that You may have stored on the Platform, may not be retrieved later. DSYH shall be under no obligation to return the information or data to you.</li>
                                        </ul>
                                        </p>

                                        <div class="header-m"> 10. GOVERNING LAW </div>
                                        <p class="Text-justify">

                                            This TOS is governed and construed in accordance with the laws of India. The courts in Delhi alone shall have exclusive jurisdiction to hear disputes arising out of the TOS without any reference to the conflict of law provisions. The Platform is made available to You by DSYH from its offices in India. You agree that: (i) the Services shall be deemed solely based in India; and (ii) the use of the Platform and the Services do not give rise to personal jurisdiction over DSYH, either specific or general, in jurisdictions other than India. You agree that the laws of India, excluding India’s choice of law rules, will apply to these TOS and to the provision of Services by DSYH and Your use of the same. DSYH makes no representation that the Platform and the Services are appropriate or available for use at other locations outside India. Access to the Platform from jurisdictions where the Services are illegal is prohibited. DSYH reserves the right to block access to the Platform by certain international users. If You access the Platform from a location outside India, You are responsible for compliance with all applicable local laws.
                                        </p>

                                        <div class="header-m"> 11. FORCE MAJEURE</div>
                                        <p class="Text-justify">

                                            DSYH shall be under no liability whatsoever in case of occurrence of a Force Majeure event, including in case of non-availability of any portion of the Platform and/or Services occasioned by act of God, war, disease, revolution, riot, civil commotion, strike, lockout, flood, fire, failure of any public utility, man-made disaster, infrastructure failure, technology outages, failure of technology integration of partners or any other cause whatsoever, beyond the control of DSYH. Further, in case of a force majeure event, DSYH shall not be liable for any breach of security or loss of data uploaded by You to the Platform.
                                        </p>

                                        <div class="header-m">   12. WAIVER</div>
                                        <p class="Text-justify">
                                            Any failure by DSYH to enforce the TOS, for whatever reason, shall not necessarily be construed as a waiver of any right to do so at any time.
                                        </p>

                                        <div class="header-m">  13. SEVERABILITY</div>
                                        <p class="Text-justify">
                                            If any of the provisions of this TOS are deemed invalid, void, or for any reason unenforceable, that part of the TOS will be deemed severable and will not affect the validity and enforceability of any remaining provisions of the TOS.
                                        </p>

                                        <div class="header-m">   14. ENTIRE AGREEMENT</div>
                                        <p class="Text-justify">
                                            The TOS as amended from time to time, along with the Privacy Policy and other related policies made available from time to time, constitutes the entire agreement and supersedes all prior understandings between the parties relating to the subject matter herein.
                                        </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                </div>
                            </div>
                        </div>
                    </div>








                    <script>
                                  (function (i, s, o, g, r, a, m) {
                                      i['GoogleAnalyticsObject'] = r;
                                      i[r] = i[r] || function () {
                                          (i[r].q = i[r].q || []).push(arguments)
                                      }, i[r].l = 1 * new Date();
                                      a = s.createElement(o),
                                              m = s.getElementsByTagName(o)[0];
                                      a.async = 1;
                                      a.src = g;
                                      m.parentNode.insertBefore(a, m)
                                  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                  ga('create', 'UA-71387415-1', 'auto');
                                  ga('send', 'pageview');</script>


                    <script>



                        function setFocus() {
                            document.getElementById("c_name").focus();
                        }



                        $('body').on('click', '.first', function () {
                            $('.first').removeClass('second');
                            $(this).addClass('second');
                            var dataatt = $(this).data('tid');
                            $("#ptid").val(dataatt);
                            $("#51").removeClass("xmas");
                            setFocus();
                        });
                        $("#51").addClass("xmas");
                        $("body").on('submit', "#registerationform", function (e) {

                            var phonenumber = $("#c_mob").val();
                            if (phonenumber.length == 10) {

                            } else {
                                alert("Please enter valid phone number");
                                return false;
                            }
                            var formval = $("#ptid").val();
                            if (formval == '' || formval == null) {
                                e.preventDefault();
                                document.getElementById('allerrors').style.display = "block";
                                return false;
                            } else {
                                document.getElementById('allerrors').style.display = "none";
                            }
                        });</script>



                    <script>
                        $(function () {
                            $('.submit').click(function () {
                                $("label .asterisk").hide();
                                var empty = $(".required").filter(function () {
                                    return !this.value;
                                })
                                        .prev().find(".asterisk").show();
                                if (empty.length)
                                    return false; //uh oh, one was empty!
                                $('.right').stop().animate({scrollTop: 0}, {duration: 1500, easing: 'easeOutQuart'});
                            });
                        });</script>

                    <script>
                        var createAllErrors = function () {
                            var form = $(this),
                                    errorList = $("ul.errorMessages", form);
                            var showAllErrorMessages = function () {
                                errorList.empty(0);
                                // Find all invalid fields within the form.
                                var invalidFields = form.find(":invalid").each(function (index, node) {

                                    // Find the field's corresponding label
                                    var label = $("label[for=" + node.id + "] "),
                                            // Opera incorrectly does not fill the validationMessage property.
                                            message = node.validationMessage || 'Invalid value.';
                                    errorList
                                            .show()
                                            .append("<li style='text-align: left;'><span style='text-align:left;/* text-shadow: 0px 1px 0px rgba(64, 64, 64, 0.32); */display: block;padding: 10px 0px 10px 0px;'>" + label.html() + "</span> " + "</li>");
          //                 normal_alert(label.html());

                                });
                            };
                            // Support Safari
                            form.on("submit", function (event) {
                                if (this.checkValidity && !this.checkValidity()) {
                                    $(this).find(":invalid").first().focus();
                                    event.preventDefault();
                                }


                                Cvalue = $('#c_name').val().trim();
                                if (Cvalue == '') {
                                    $('#c_name').find(Cvalue).first().focus();
                                    event.preventDefault();
                                }
                            });
                            $("input[type=submit], button:not([type=button])", form)
                                    .on("click", showAllErrorMessages);
                            $("input", form).on("keypress", function (event) {
                                var type = $(this).attr("type");
                                if (/date|email|month|number|search|tel|text|time|url|week/.test(type)
                                        && event.keyCode == 13) {
                                    showAllErrorMessages();
                                }
                            });
                        };
                        $("form").each(createAllErrors);</script>

                    <script type="text/javascript">
                        function blockSpecialChar(event) {
                            var k = event.which;
                            return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57));
                        }
                    </script>


                    <script>
                        if ($('input:invalid')) {


                            return false;
                        }

                        // text align

                        $('#c_aliasname').onclick(function () {

                            $('#c_aliasname').css('text-align':'right!important');
                        });
                    </script>

                    <script>

                    </script>




                    <style>
                        input:valid {
                            border-bottom: 1px solid #8BC34A !important;
                        }



                        /* webkit solution */
                        #c_aliasname::-webkit-input-placeholder { text-align:left; }
                        /* mozilla solution */
                        #c_aliasname:-moz-placeholder { text-align:left; }


                        pre {
                            display: block;
                            padding: 5.5px;
                            margin: 0 0 10px;
                            font-size: 13px;
                            line-height: 1.42857143;
                            color: #333;
                            word-break: break-all;
                            word-wrap: break-word;
                            background-color: #f5f5f5;
                            border: 1px solid #ccc;
                            border-radius: 4px;
                        }


                        @media only screen and (max-width: 1770px) {


                            .errorMessages {
                                position: static!important;
                            }

                        }
                    </style>


                    <style>
                        .schedule:hover {
                            left: 0px;
                            box-shadow: 3px 2px 20px 0px rgba(0, 0, 0, 0.29);
                            color: black;
                            letter-spacing: 1px;
                            font-weight: 600;
                        }
                        .schedule {
                            left: -162px;
                            padding: 10px;
                            background: #e8c447 url('/img/schedule-icon.png') center right no-repeat;
                            background-size: 56px;
                            color: white;
                            padding-right: 54px;
                            border-top-right-radius: 4px;
                            border-bottom-right-radius: 4px;
                            position: fixed;
                            transition: 0.3s;
                            text-decoration: none;
                            font-family: segoe ui;
                            font-size: 14px;
                            image-rendering: pixelated;
                            bottom: 1.5%;
                            text-shadow: 0px 1px rgba(0, 0, 0, 0.24);
                            box-shadow: 4px 4px 4px -3px rgba(0, 0, 0, 0.29);
                        }


                    </style>

                    <a href="https://dsyh.appointy.com/" target="_blank" class="schedule">Schedule an Appointment</a>
<script src="/js/typewriting.min.js"></script>
<script src="/js/mainFortype.js" type="text/javascript"></script>


                <script>
function myFunction() {
    setTimeout(function(){

        $('#help').slideDown();
    }, 4000);
}

myFunction();
</script>

                </body>
                    </html>
