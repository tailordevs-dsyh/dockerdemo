

//addnewChannel
 filename="getConnectorData.php";
 
function addnewChannel() {
  datastring = $("#addNewChannel").serialize() + "&listcha=1";
  $('#btnsubmit123').attr('disabled');
  alert(datastring);
  $.ajax({
    type: 'POST',
    url: filename,
    data: datastring,
    cache: true,
    success: function (data) {
//        alert(data);
            
      $('#btnsubmit123').removeAttr('disabled', 'disabled');
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
    error: function (data) {
//        alert(data);
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });

}

//getChanneloption

function getChanneloption() {
  var formData = {hdval: hdval};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#option').html(data).fadeIn(4000);
      $("#allgreat").show();

      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//getChannelDetail

function getChannelDetail(opt) {
  var formData1 = {channelid: hdval, opt: opt};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData1,
    cache: true,
    success: function (data) {
      $('#login').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//getConnectorDetail

function getConnectorDetail(uid, id) {
  var formdata = {id1: hdval, uid: uid, columnid: id};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formdata,
    cache: true,
    success: function (data) {
      $('#showAccount').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//getSYNC

function getSYNC() {
  var formData = {getSync: 'sync', affiliate_id: hdval};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#Sync').html(data).fadeIn(5000);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//     getAutoship

function getAutoship() {
  var formData = {getautoShip: 'autoShip', affiliateid: hdval};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#autoshipon').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//      saveAutoShip

function saveAutoShip() {
  var formData = {autoShipval: 'autoship', chaid: hdval};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      //                                    alert(data);
      //                            $('#autoship-msg').html(data);
      //                                    $("#allgreat").show();
      setTimeout(function () {
        //                self.location = "/branch_dashboard_v3.php";
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
        //                alert('ERROR - Please Enter Correct Info.!');
        //                self.location = "/modal.php";
      }, 2000);
    }
  });
}

//    channelactive

function channelactive(uid, valueofActive) {

  var formData = {channelactive: valueofActive, affid2: hdval, uid2: uid};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#autoship-msg').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//      connectorUpdate

function connectorUpdate(olduid) {
  $('.btn-danger').attr('disabled', 'disabled');
  colid = $('.useremail').attr('data-id');

  if (hdval == "17") {
    appname = $('#appname').val().trim();
    merchantid = $('#merchantid').val().trim();
    authtoken = $("#authtoken").val().trim();
    uid = $('#uid').val().trim();
    pwd = $('#pwd').val().trim();

    if (appname == "" || merchantid == "" || authtoken == "" || uid == "" || pwd == "" || colid == "") {
      $('#error_msg3').show();
      error_alert('Please fill all the fields! ');
    }
    else {
      $('#error_msg3').hide();
    }

    var formData = {cid: hdval, olduid: olduid, appname: appname, merchantid: merchantid, authtoken: authtoken, uid: uid, pwd: pwd, colid: colid};
  } else {
    uid = $('#uid').val().trim();
    pwd = $('#pwd').val().trim();
    if (uid == "" || pwd == "" || colid == "") {
      $('#error_msg4').show();
      error_alert('Please fill all the fields! ');
    }
    else {
      $('#error_msg4').hide();
    }
    var formData = {cid: hdval, uid: uid, pwd: pwd, olduid: olduid, colid: colid};
  }
  $("#sdloader").show();
  $(this).hide();
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('.btn-danger').removeAttr('disabled', 'disabled');
      getChannelDetail('Connector');
      $('#msg').html(data);
      $("#sdloader").hide();
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//    connectorInsert

function connectorInsert() {
  $('.btn-danger').attr('disabled', 'disabled');
  if (hdval == "17") {
    appname1 = $('#appname1').val().trim();
    merchantid1 = $('#merchantid1').val().trim();
    authtoken1 = $("#authtoken1").val().trim();
    uid1 = $('#uid1').val().trim();
    pwd1 = $('#pwd1').val().trim();
    if (appname1 == "" || merchantid1 == "" || authtoken1 == "" || uid1 == "" || pwd1 == "") {
      $('#error_msg1').show();
    }
    else {
      $('#error_msg1').hide();
    }
    var formData = {cid1: hdval, appname1: appname1, merchantid1: merchantid1, authtoken1: authtoken1, uid1: uid1, pwd1: pwd1};
  } else {
    uid1 = $('#uid1').val().trim();
    pwd1 = $('#pwd1').val().trim();
    if (uid1 == "" || pwd1 == "") {
      $('#error_msg').show();
    }
    else {
      $('#error_msg').hide();
    }
    var formData = {cid1: hdval, uid1: uid1, pwd1: pwd1};
  }

  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('.btn-danger').removeAttr('disabled', 'disabled');
      getChannelDetail('Connector');
      $('#msg1').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//      addAccount

function addAccount() {
  $('.btn-danger').attr('disabled', 'disabled');

  var formData = {channel_id: hdval};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('.btn-danger').removeAttr('disabled', 'disabled');
//                            alert(data);
      info_alert('Create a New Account & get Connected ');
      $('#showForm').html(data);
//                                    $("#allgreat").show();
      setTimeout(function () {
        //                self.location = "/branch_dashboard_v3.php";
      }, 5000);
    },
    error: function (data) {
      error_alert('Something went wrong!?!');

      setTimeout(function () {
        //                alert('ERROR - Please Enter Correct Info.!');
        //                self.location = "/modal.php";
      }, 2000);
    }
  });
}

//    getcustomInvoiceSeries

function getcustomInvoiceSeries(affid) {
  var formData = {c_code: affid};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
//                            alert(data);
//            info_alert('Create a New Account & get Connected ');
      $('#custome-invoiceData').html(data);
//                                    $("#allgreat").show();
      setTimeout(function () {
        //                self.location = "/branch_dashboard_v3.php";
      }, 5000);
    },
    error: function (data) {
      error_alert('Something went wrong!?!');

      setTimeout(function () {
        //                alert('ERROR - Please Enter Correct Info.!');
        //                self.location = "/modal.php";
      }, 2000);
    }
  });
}

//    defaultInvoice

function defaultInvoice() {
  var formData = {default: 'default', affid: hdval};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#defaultmsg').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//    customInvoice

function customInvoice() {
  
//  alert('qwerty');    
  
  warehouse = $('#warehouse').val();
  channel = $('#channel').val();
  month = $('#month').val();
  series = $('#series').val();
  if (channel == "" || month == "" || series == "") {
    $('#errormsg1').show();
  } else {
    var formData = {affid: hdval, custom: 'custom', channel: channel, warehouse: warehouse, month: month, series: series};
    $('#errormsg1').hide();
  }
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#custommsg').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}


//    syncTheStatus

function syncTheStatus(statusid) {
 var formData = {affid: hdval, statuscheck: 'statuscheck',userIdForSync : statusid};
 $.ajax({
   type: 'POST',
   url: filename,
   data: formData,
   cache: true,
   success: function (data) {
     $('#Sync-btns').html(data);
     setTimeout(function () {
     }, 5000);
   },
   error: function (data) {
     $("#thanks").hide();
     $("#error").show().fadeIn(1000);
     setTimeout(function () {
     }, 2000);
   }
 });
}

//      resetChannel

function resetChannel() {
  document.getElementById("addNewChannel").reset();
  $('input:checkbox').removeAttr('checked');
  $('#SaveTheChnn').attr('disabled', 'disabled');
}

//    add remove disbled attr button

$('.chkbx').click(function () {
  $('#SaveTheChnn').removeAttr('disabled', 'disabled');
});

//    resetForm

function resetForm($form){
  $form.find('input:text, input:password, input:file, select, textarea').val('');
  $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}

//    On click .common (<a>) funtion

$('body').click(function () {
  $('#nickname').parent().text(oriVal);
  $('#nickname').remove();
});


  $('.common').click(function () {
  $('.common').removeClass('channelselected');
  $(this).addClass('channelselected');
  hdval = $(this).attr('id');
  chh_name = $(this).attr('data-chh');
  $('#myhidden').val(hdval);
  getChanneloption(hdval);
  affname = $('#AffName').text($(this).text());
  var className = $('.myclass').attr('class');
  affid = $(this).attr('id');
  $('#activeicon').removeClass($('#activeicon').attr('class'));
  $('#channelbar').show();
  $('#form-sync').hide();
  $('#divname').hide();
  $('#connectorDetail').hide();
  $('#connectorlogin').hide();
  $('#sync-detail').hide();
  $('#form-invoice').hide();
  $('#form-customseries').hide();
  $('#form-defaultseries').hide();
  $('#form-autoship').hide();
  $('#connectorlogin').hide();
  $('#form-activeIn').hide();
  $('#add_act').hide();
  $('a').removeClass('linkactive');
});


$('a').click(function () {
  if ($(this).attr('id') == 'refresh') {
//         $('#channelbar').hide();
//         $('#connectorlogin').hide();
//         $('#form-invoice').hide();
//         $('#form-customseries').hide();
//         $('#form-defaultseries').hide();
//         $('#form-autoship').hide();
//         $('#add_act').hide();
//         $('form-sync').hide();
    location.reload();
//       customInvoice();
//       defaultInvoice();
//       getcustomInvoiceSeries(affid);
//       updatechannelname(newName,oriName);
//       getChanneloption();
//       getChannelDetail();
//       getConnectorDetail();
//       getAutoship();
//       channelactive(uid, valueofActive);
  }
});

//    #form-sync link to open #sync-detail

$('#form-sync').on('click', 'a', function () {
  if ($(this).attr('id') == 'Sync_Id') {
    $('#connectorDetail').hide();
    $('#sync-detail').show();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#add_act').hide();
  }
});


$('#connectorlogin').on('click', 'a', function () {
  if ($(this).attr('id') == 'test1') {
    $('#connectorDetail').show();
    $('#form-invoice').hide();
    $('#form-sync').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#add_act').hide();
  }
});


$('#connectorlogin').on('click', 'input', function () {
  if ($(this).attr('id') == 'addActBtn') {
    $('#add_act').show();
    $('#form-sync').hide();
    $('#connectorDetail').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('.Userselected').removeClass('Userselected');
  }

});

// for adding loader
$('#Sync-btns').on('click', '.syncamz', function () {
  $(this).addClass('m-progress disabled');

});

// for removing loader

$('#Sync-btns').on('blur', '.syncamz', function () {
  $(this).removeClass('m-progress disabled');
});

// Userselected css for #form-sync

$('#form-sync').on('click', 'a', function () {
  $('.forlist').removeClass('Userselected');
  $(this).addClass('Userselected');
});

// Userselected css for #connectorlogin

$('#connectorlogin').on('click', 'a', function () {
  $('.forlist').removeClass('Userselected');
  $(this).addClass('Userselected');
});

// For channelbar Link Open

$('#channelbar').on('click', 'a', function () {
  $('.forlist').removeClass('channelselected');
  $('#divname').hide();

  if ($(this).attr('id') == 'Connector') {
    $(this).addClass('channelselected');
    $('#connectorDetail').hide();
    $('#connectorlogin').show();
    $('#form-sync').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#form-activeIn').hide();
    $('#add_act').hide();
    $('#sync-detail').hide();
  }
  if ($(this).attr('id') == 'test1') {
    $('#connectorDetail').show();
    $('#form-sync').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#add_act').hide();
    $('#sync-detail').hide();
  }

  if ($(this).attr('id') == 'Invoice Series') {
    $(this).addClass('channelselected');
    $('#form-invoice').show();
    $('#connectorlogin').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#form-sync').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#add_act').hide();
    $('#sync-detail').hide();
    

    
    
    
  }
  if ($(this).attr('id') == 'Auto-ship') {
    $(this).addClass('channelselected');
    $('#form-autoship').show();
    $('#form-sync').hide();
    $('#connectorlogin').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#add_act').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#sync-detail').hide();
  }

  if ($(this).attr('id') == 'SyncID') {
    $(this).addClass('channelselected');
    $('#form-sync').show();
    $('#form-autoship').hide();
    $('#connectorlogin').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#add_act').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#sync-detail').hide();
  }

  if ($(this).attr('id') == 'ActiveIn') {
    $(this).addClass('channelselected');
    $('#form-sync').hide();
    $('#form-activeIn').show();
    $('#connectorlogin').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#connectorDetail').hide();
    $('#form-autoship').hide();
    $('#add_act').hide();
    $('#sync-detail').hide();
  }
});

// For Radio Buttons [Default] & [Custom] Invoice



$('input[name=Cinvoice]:radio').on("click", function () {
        var name = $(this).data('name');
        var val = $(this).val();
        if (val == 'custom') {
          $('#collbundle').collapse("show");
          $("#form-customseries").show();
          $("#form-defaultseries").hide();
          getcustomInvoiceSeries(hdval);
        } else if (val == 'default') {
          $('#collbundle').collapse("hide");
          $("#collbundle").removeClass('in');
          $("#form-customseries").hide();
          $("#form-defaultseries").show();
        }
      });



//        updatechannelname

function updatechannelname(newName, oriName, myaffid) {
  var formData = {newName: newName, oriName: oriName, myid: myaffid};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#showerror').html(data);
    },
    error: function (data) {
    }
  });
}



function blockSpecialChar(event) {
  var k = event.keyCode;
  if (!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))) {
    event.returnValue = false;
    return true;
  }
  event.returnValue = true;
}

$('.btn').on('click', function () {
  var $this = $(this);
  $this.button('loading');
  setTimeout(function () {
    $this.button('reset');   // to reset the button  
  }, 1000);
});


//getInvoice


function getInvoice() {
  var formData = {affid_getInv: hdval};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
//        alert(data);
      $('#inv_data').html(data);
    },
    error: function (data) {
      alert(data);
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
    }
  });
}



 $("body").on('keydown', 'section', function (e) {
if (e.keyCode == 27) {
          $('#channelbar').hide();
          $('#form-sync').hide();
          $('#divname').hide();
          $('#connectorDetail').hide();
          $('#connectorlogin').hide();
          $('#sync-detail').hide();
          $('#form-invoice').hide();
          $('#form-customseries').hide();
          $('#form-defaultseries').hide();
          $('#form-autoship').hide();
          $('#connectorlogin').hide();
          $('#form-activeIn').hide();
          $('#add_act').hide();
        } 
      });