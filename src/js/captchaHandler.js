


function submitForm()
{
	var params = "callFor=GetLoginErrorDetails&affId=8";
	var xhr=null;

   	try
   	{
		xhr = new XMLHttpRequest(); 
   	} 
	catch(e)
	{ 
     		try 
    		{ 
			xhr = new ActiveXObject("Msxml2.XMLHTTP"); 
		} 
     		catch (e2)
    		{ 
       			try
			{
				xhr = new ActiveXObject("Microsoft.XMLHTTP"); 
			} 
       			catch (e) 
			{}
    		}
  	}

 	xhr.onreadystatechange = function()
   	{ 
     		data="Wait server...";      
     		if(xhr.readyState == 4)
     		{
        		if(xhr.status == 200)
        		{ 
				data = xhr.responseText;
        		} 
       			else 
        		{ 
				data = "Error: returned status code " + xhr.status + " " + xhr.statusText;
       			} 
    		}
    		postMessage(data.toString());
 	}; 

	xhr.open("POST", "../dbActionsOnPost.php", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send(params);
	setTimeout("submitForm()",3000);

}

submitForm();
