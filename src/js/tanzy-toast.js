var Danger_msg_alert = '';
var Success_msg_alert = '';
var Normal_msg_alert = '';
var Info_msg_alert = '';


function snakbar_alert(msg,classCss) {
    var x = document.getElementById("snackbar");
//  x.innerHTML(msg);
    document.getElementById("snackbar").innerHTML = msg;
    x.className = "show ";
    x.className += classCss;

    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

function success_alert(msg) {

    if (Success_msg_alert != msg || !(document.getElementById('forSuccess'))) {
        var item = $('<div  id="forSuccess" class="notification success"><span>' + msg + '</span></div>');
        $("#toastem").append($(item));
        $(item).animate({"right": "12px"}, "fast");
        setInterval(function () {
            $(item).animate({"right": "-400px"}, function () {
                $(item).remove();
            });
        }, 8000);
        Success_msg_alert = msg;

    }
}


function danger_alert(msg) {
    if (Danger_msg_alert != msg || !(document.getElementById('forDanger'))) {
        var item1 = $('<div id="forDanger" class="notification danger"><span>' + msg + '</span></div>');

        $("#toastem").append($(item1));
        $(item1).animate({"right": "12px"}, "fast");
        setInterval(function () {
            $(item1).animate({"right": "-400px"}, function () {
                $(item1).remove();
            });
        }, 8000);
        Danger_msg_alert = msg;
    }
}

function info_alert(msg) {
    if (Info_msg_alert != msg || !(document.getElementById('forInfo'))) {
        var item2 = $('<div id="forInfo" class="notification info"><span>' + msg + '</span></div>');
        $("#toastem").append($(item2));
        $(item2).animate({"right": "12px"}, "fast");
        setInterval(function () {
            $(item2).animate({"right": "-400px"}, function () {
                $(item2).remove();
            });
        }, 8000);
        Info_msg_alert = msg;
    }
}

function normal_alert(msg) {
    if (Normal_msg_alert != msg || !(document.getElementById('forNormal'))) {
        var item3 = $('<div id="forNormal" class="notification normal"><span>' + msg + '</span></div>');
        $("#toastem").append($(item3));
        $(item3).animate({"right": "12px"}, "fast");
        setInterval(function () {
            $(item3).animate({"right": "-400px"}, function () {
                $(item3).remove();
            });
        }, 8000);

        Normal_msg_alert = msg;
    }
}
