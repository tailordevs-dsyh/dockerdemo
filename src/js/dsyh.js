/* This contains all the js which are manually created for use in dsyh application. */



function DatePicker(elementId){
	
	var today = new Date();
	var minDate = '1/1/'+(today.getFullYear()-4);
	var maxDate = '31/12/'+today.getFullYear();

    var cb = function (start, end, label) {
        $(elementId+' span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    var optionSet1 = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: minDate,
        maxDate: maxDate,
        dateLimit: {days: 2400},
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        buttonClasses: ['btn '],
        applyClass: 'btn-primary btn-xs',
        cancelClass: 'btn-normal btn-xs',
        format: 'DD/MM/YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Apply',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    };

    $(elementId+' span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $(elementId).daterangepicker(optionSet1, cb);

    $('#options1').click(function () {
        $(elementId).data('daterangepicker').setOptions(optionSet1, cb);
    });

    $('#destroy').click(function () {
        $(elementId).data('daterangepicker').remove();
    });
} // DatePicker Ends here
