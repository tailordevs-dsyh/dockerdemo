// THIS FILE IS USED FOR channelconnector.php


//addnewChannel
filename = "getConnectorData.php";

/////////////////////////////////////////////////////////////////// THIS CODE IS FOR RENAMING ACCOUNT UID/ALIAS ///////////////////////////////////////////////////////////////////////
var oriVal;

$("body").on('dblclick', '.rename', function () {
//  alert('click');
  oriVal = $(this).text();
  myid = $(this).attr('id');
  oriVal = $(this).text();
  $(this).text("");
  oldTextval = "<input type='text' id='nickname' class='"+myid+"'  onkeypress='return blockSpecialChar();' maxlength='20' placeholder='Custom name'  style='text-transform: capitalize;'> ";
  $(oldTextval).appendTo(this).focus();
});

$("body").on('blur', '#nickname', function (e) {
        $(this).focus();
//      $('section').addClass('noPointerEvent');
      });

$("body").on('keydown', '#nickname', function (e) {      // escape key maps to keycode `27` || On esc-key all the blocks will be closed..
    if (e.keyCode == 27) {
      var $this = $(this);
      $this.parent().text(oriVal);
      $('#channelbar').hide();
      $('#form-sync').hide();
      $('#divname').hide();
      $('#RPOBlist').hide();
      $('#connectorDetail').hide();
      $('#connectorlogin').hide();
      $('#sync-detail').hide();
      $('#form-invoice').hide();
      $('#form-customseries').hide();
      $('#form-defaultseries').hide();
      $('#form-autoship').hide();
      $('#connectorlogin').hide();
      $('#form-activeIn').hide();
      $('#add_act').hide();
    }

    if (e.keyCode == 13) {
        var $this = $(this);
        if ($this.val()=="") {
          $this.parent().text($this.val() || oriVal);
          $(this).val(oriVal);
          if ($this.parent().text() == "") {
            $(this).focus();
          }
        } else {

        if ($this.val().trim() == '') {
            l = $this.val().trim();
        } else {
            newname = $this.val();
            myid1 = $(this).attr('class');
            $this.parent().text($this.val() || oriVal);
            $this.remove(); // Don't just hide, remove the element.
            updatechannelname(newname, oriVal, myid1);
            $('#RPOBlist').hide();
            $('#channelbar').hide();
//          location.reload();
//            alert(newname+oriVal+myid1);
        }
      }
    }
});


// adding new channels

function addnewChannel() {
  datastring = $("#addNewChannel").serialize() + "&listcha=1";
  $('#btnsubmit123').attr('disabled');
//  alert(datastring);
  $.ajax({
    type: 'POST',
    url: filename,
    data: datastring,
    cache: true,
    success: function (data) {
      $('#btnsubmit123').removeAttr('disabled', 'disabled');
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//getChanneloption

function getChanneloption() {
  $('#option').hide();
  $('#channelbar_loader').show();
  var UseridInput = $('#UseridHidden').val();
  var formData = {our_Aff_id: our_Aff_id,myUid: UseridInput};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
//      info_alert('im in getChanneloption');
      $('#channelbar_loader').hide();
      $('#option').html(data).show();
      $("#allgreat").show();

      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//getChannelDetail

function getChannelDetail(opt) {
  $('#chlogin').hide();
  $('#connectorLogin_loader').show();
  var formData1 = {channelid: our_Aff_id, opt: opt};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData1,
    cache: true,
    success: function (data) {

      $('#connectorLogin_loader').hide();
      $('#chlogin').show();

      $('#login').html(data).show();
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//getConnectorDetail

function getConnectorDetail(uid, id) {

  $('#connDetail_table').hide();
  $('#connectorDetail_loader').show();
  var formdata = {id1: our_Aff_id, uid: uid, columnid: id};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formdata,
    cache: true,
    success: function (data) {
      $('#connDetail_table').show();
      $('#connectorDetail_loader').hide();
      $('#showAccount').html(data).show();
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//getSYNC

function getSYNC() {
  $('#sync_table').hide();
  $('#Sync').hide();
  $('#sync_loader').show();
  var formData = {getSync: 'sync', affiliate_id: our_Aff_id};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#sync_loader').hide();
      $('#sync_table').show();

      $('#Sync').html(data).show();
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//     getAutoship

function getAutoship() {
  var formData = {getautoShip: 'autoShip', affiliateid: our_Aff_id};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#autoshipon').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}


//      saveAutoShip

function saveAutoShip() {
  var formData = {autoShipval: 'autoship', chaid: our_Aff_id};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      //                                    alert(data);
      //                            $('#autoship-msg').html(data);
      //                                    $("#allgreat").show();
      setTimeout(function () {
        //                self.location = "/branch_dashboard_v3.php";
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
        //                alert('ERROR - Please Enter Correct Info.!');
        //                self.location = "/modal.php";
      }, 2000);
    }
  });
}


//      saveInv_Sync

function saveInv_Sync(id, val, fkQty) {
    var val = val;
    var id = id;
    var fkQty = fkQty;
    var userId = $('#UseridHidden').val();

    var formData = {SaveInvSync: val, affid: id, fkQty: fkQty,userKiId:userId};
    $.ajax({
        type: 'POST',
        url: filename,
        data: formData,
        cache: true,
        success: function (data) {
            $('#InvSync_msg').html(data);
//        alert(data);
        },
        error: function (data) {
            alert('Error');
        }
    });
}

// Pura Inventory Sync Yeh handle karta hai

function pura_Sync_save_karunga(value) {
    var valueMe = value;
    var formData = {sab_save_kar: valueMe};
    $.ajax({
        type: 'POST',
        url: filename,
        data: formData,
        cache: true,
        success: function (data) {
            var s = $('#result').html(data);
            getChanneloption();
            $('#form-InvSync').hide();
//      alert('succ');
        },
        error: function (data) {
            alert('Error');
        }
    });
}

//      Inventory Sync function

function Inv_Sync() {
  var userId = $('#UseridHidden').val();
    var AffIdHidden = $('#AffIdHidden').val();
    var formData = {switchOfInvSync: 'switchOfInvSync', id: AffIdHidden, userKiId:userId};

    $.ajax({
        type: 'POST',
        url: filename,
        data: formData,
        cache: true,
        success: function (data) {
            $('#InvSyncData').html(data);
        },
        error: function (data) {
            alert('Error!');
        }
    });
}


//      channelactive

function channelactive(uid, valueofActive) {

  var formData = {channelactive: valueofActive, affid2: our_Aff_id, uid2: uid};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      channellist();
      $('#autoship-msg').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//      connectorUpdate

function connectorUpdate(olduid) {

//  SelectApob2 = SelectApob;
//alert('SelectApob2');
  $('.btn-danger').attr('disabled', 'disabled');
  colid = $('.useremail').attr('data-id');

  if (our_Aff_id == "17") {
    appname = $('#appname').val().trim();
    merchantid = $('#merchantid').val().trim();
    authtoken = $("#authtoken").val().trim();
    uid = $('#uid').val().trim();
    pwd = $('#pwd').val().trim();

    if (appname == "" || merchantid == "" || authtoken == "" || uid == "" || pwd == "" || colid == "") {
      $('#error_msg3').show();
      danger_alert('Please fill all the fields! ');
    }
    else {
      $('#error_msg3').hide();
    }

    var formData = {cid: our_Aff_id, olduid: olduid, appname: appname, merchantid: merchantid, authtoken: authtoken, uid: uid, pwd: pwd, colid: colid};
  } else {
    uid = $('#uid').val().trim();
    pwd = $('#pwd').val().trim();
    if (uid == "" || pwd == "" || colid == "") {
      $('#error_msg4').show();
      danger_alert('Please fill all the fields! ');
    }
    else {
      $('#error_msg4').hide();
    }
      var formData = {cid: our_Aff_id, uid: uid, pwd: pwd, olduid: olduid, colid: colid};
  }
  $("#sdloader").show();
  $(this).hide();



  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#msg').html(data);
      $("#sdloader").hide();
      UserName = $('#UserNameHidden').val(uid);
      UserNameInput = $('#UserNameHidden').val();
      $('.btn-danger').removeAttr('disabled', 'disabled');
//    getChannelDetail('Connector');
      getUserid(our_Aff_id);
      var datauser = $('#'+our_Aff_id).text();
//    alert(UsernameInput + '--' + uid);
//    str123='.common #'+our_Aff_id;
      dataid = 'a[data-userid='+colid+']';
//    alert(dataid);
      mydatav = $(dataid);
//    alert(dataid +'\n'+ UsernameInput);
//    $('a[data-userid="138"]').addClass('channelselected');
//    getChanneloption();

     setTimeout(function () {
         $(dataid).addClass('channelselected');
      }, 50).fadeIn();
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
       dataid = 'a[data-userid='+colid+']';
    $(dataid).addClass('channelselected');
    }
  });
}

//    connectorInsert

function connectorInsert() {
  SelectApob1 = SelectApob;
  $("#sdloader").show();
  $('.btn-danger').attr('disabled', 'disabled');



  var error="";
  if (our_Aff_id == "17") {
    appname1 = $('#appname1').val().trim();
    merchantid1 = $('#merchantid1').val().trim();
    authtoken1 = $("#authtoken1").val().trim();
     uid1 = $('#uid1').val().trim();
    pwd1 = $('#pwd1').val().trim();


    if (appname1 == "" || merchantid1 == "" || authtoken1 == "" || uid1 == "" || pwd1 == "") {
      $('#msg1').hide();
      $('#error_msg1').show();
      $("#sdloader").hide();
      $('#newaccbtnAMZ').removeAttr('disabled', true);
      error="1";
    }
    else {
      $('#error_msg1').hide();
      error="0";
    }
    var formData = {cid1: our_Aff_id, appname1: appname1, merchantid1: merchantid1, authtoken1: authtoken1, uid1: uid1, pwd1: pwd1, selectAPOB: SelectApob1};
  } else {
    uid1 = $('#uid1').val().trim();
    pwd1 = $('#pwd1').val().trim();
    if (uid1 == "" || pwd1 == "") {
      $('#msg1').hide();
      $('#error_msg').show();
      $("#sdloader").hide();
      $('.btn-danger').removeAttr('disabled', 'disabled');
      error="1";
    }
    else {
      $('#error_msg').hide();
      error="0";
    }
    var formData = {cid1: our_Aff_id, uid1: uid1, pwd1: pwd1, selectAPOB: SelectApob1};
  }
    $('#apobSave').attr('disabled',true);
    $('#newaccbtn').attr('disabled',true);
    $('#APOBlist').attr('disabled',true);
//    danger_alert('Error1');
//    alert(error);
  if(error ==="0"){
//    alert("if condition");
    $.ajax({
      type: 'POST',
      url: filename,
      data: formData,
      cache: true,
      success: function (data) {
        $('#newaccbtn').attr('disabled',true);
        $('#msg1').html(data);
//      alert(data);


        $('#msg1').show();
        $("#sdloader").hide();
        getUserid(our_Aff_id);
        $('#UserNameHidden').val(uid1);
        UserNameInput = $('#UserNameHidden').val();
        UserIdInput = $('#UseridHidden').val();
        getChanneloption(UserIdInput, UserNameInput);
//    $(dataid).addClass('channelselected');
//    $('#channelbar').hide();
//    $('#RPOBlist').hide();
//    $('#connectorDetail').hide();
//    getChannelDetail('Connector');
//    getChanneloption();
       if(data.trim().indexof('already')){
          info_alert(data);
        }
      },
      error: function (data) {
        danger_alert('Something went wrong!');
        alert(data);
        $("#thanks").hide();
        $("#error").show().fadeIn(1000);
        setTimeout(function () {
        }, 2000);
      }
  });
  }
  else{
    $('#newaccbtn').attr('disabled',false);
    danger_alert('Please fill correct details..');
  }

//  alert(SelectApob1);

}

//      addAccount

function addAccount() {
  $('.btn-danger').attr('disabled', 'disabled');

  var formData = {newacc: our_Aff_id};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      getChanneloption();
      $('.btn-danger').removeAttr('disabled', 'disabled');
//                            alert(data);
      info_alert('Create a New Account & get Connected ');
      $('#showForm').html(data);
//                                    $("#allgreat").show();
      setTimeout(function () {
        //                self.location = "/branch_dashboard_v3.php";
      }, 5000);
    },
    error: function (data) {
      danger_alert('Something went wrong!?!');

      setTimeout(function () {
        //                alert('ERROR - Please Enter Correct Info.!');
        //                self.location = "/modal.php";
      }, 2000);
    }
  });
}

//    getcustomInvoiceSeries

function getcustomInvoiceSeries(affid) {
  var formData = {c_code: affid};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
//                            alert(data);
//            info_alert('Create a New Account & get Connected ');
      $('#custome-invoiceData').html(data);
//                                    $("#allgreat").show();
      setTimeout(function () {
        //                self.location = "/branch_dashboard_v3.php";
      }, 5000);
    },
    error: function (data) {
      danger_alert('Something went wrong!?!');

      setTimeout(function () {
        //                alert('ERROR - Please Enter Correct Info.!');
        //                self.location = "/modal.php";
      }, 2000);
    }
  });
}

//    defaultInvoice

function defaultInvoice() {
  var formData = {default: 'default', affid: our_Aff_id};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#defaultmsg').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//    customInvoice

function customInvoice() {

//  alert('qwerty');


  if ($('#ResetSeries').is(":checked")) {
    ResetSeriesV = "Y";
  } else {
    ResetSeriesV = "N";
  }

//  alert(ResetSeriesV);
  warehouse = $('#warehouse').val();
  channel = $('#channel').val();
  month = $('#month').val();
  series = $('#series').val();
  if (channel == "" || month == "" || series == "") {
    $('#errormsg1').show();
  } else {
    var formData = {affid: our_Aff_id, custom: 'custom', channel: channel, warehouse: warehouse, month: month, series: series, ResetSeries: ResetSeriesV};
    $('#errormsg1').hide();
  }
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#custommsg').html(data);
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}


//    syncTheStatus

function syncTheStatus(statusid) {
  userId = statusid;
  $('#sync_detail_loader').show();
  $('#Sync-btns').hide();
  $('#sync_detail_table').hide();
  var formData = {affid: our_Aff_id, statuscheck: 'statuscheck', userIdForSync: statusid};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#sync_detail_loader').hide();
      $('#sync_detail_table').show();
      $('#Sync-btns').html(data).show();
      setTimeout(function () {
      }, 5000);
    },
    error: function (data) {
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
      setTimeout(function () {
      }, 2000);
    }
  });
}

//      resetChannel

function resetChannel() {
  document.getElementById("addNewChannel").reset();
  $('input:checkbox').removeAttr('checked');
  $('#SaveTheChnn').attr('disabled', 'disabled');
}

//    add remove disbled attr button

$('.chkbx').click(function () {
  $('#SaveTheChnn').removeAttr('disabled', 'disabled');
});

//    resetForm

function resetForm($form) {
  $form.find('input:text, input:password, input:file, select, textarea').val('');
  $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}

//    On click .common (<a>) funtion

$('body').click(function () {
  $('#nickname').parent().text(oriVal);
  $('#nickname').remove();
});





//$('.channelnames td> #17').addClass('channelselected');
//[<a href=​"#" data-modal=​"#modal1" id=​"17" data-chh=​"Amazon VOI" class=​"common modal__trigger  channelselected" title=​"Double-click to Rename" style=​"text-transform:​ capitalize;​">​Amzon Voi                                       ​</a>​/

$('a').click(function () {
  if ($(this).attr('id') == 'refresh') {
    $('#connectorDetail').hide();
//    $('#channelbar').hide();
//    $('#connectorlogin').hide();
//    $('#form-invoice').hide();
//    $('#form-customseries').hide();
//    $('#form-defaultseries').hide();
//    $('#form-autoship').hide();
//    $('#add_act').hide();
//    $('form-sync').hide();
//    location.reload();
//    addAccount();
//    customInvoice();
//    connectorInsert();
//    defaultInvoice();
//    getcustomInvoiceSeries(affid);
//    updatechannelname(newName,oriName);
//    resetChannel();
//    channelactive(uid, valueofActive);
    getSYNC();
    getChanneloption();
    getChannelDetail('Connector');
    getConnectorDetail();
    getAutoship();
    channellist();
    addClassSelected(our_Aff_id);
    Inv_Sync();
//    syncTheStatus();
  }
});

//    #form-sync link to open #sync-detail
//
//$('#channelbar').on('click', 'a', function () {
//
//});


$('#connectorlogin').on('click', '#addActBtn', function () {
  $(this).attr('disabled', true);

});


$('#connectorlogin').on('click', 'a', function () {
  if ($(this).attr('id') == 'test1') {
    $('#connectorDetail').show();
    $('#form-invoice').hide();
    $('#form-sync').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#add_act').hide();
  }
});


$('#connectorlogin').on('click', 'input', function () {
  if ($(this).attr('id') == 'addActBtn') {
    $('#add_act').show();
    $('#form-sync').hide();
    $('#connectorDetail').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('.Userselected').removeClass('Userselected');
  }

});

// for adding loader
$('#Sync-btns').on('click', '.syncamz', function () {
  $(this).addClass('m-progress disabled');

});

// for removing loader

$('#Sync-btns').on('blur', '.syncamz', function () {
  //$(this).removeClass('m-progress disabled');
});

// Userselected css for #form-sync

$('#form-sync').on('click', 'a', function () {
  $('.forlist').removeClass('Userselected');
  $(this).addClass('Userselected');
});

// Userselected css for #connectorlogin

$('#connectorlogin').on('click', 'a', function () {
  $('.forlist').removeClass('Userselected');
  $(this).addClass('Userselected');
});

// For channelbar Link Open

$('#channelbar').on('click', 'a', function () {
  $('.forlist').removeClass('optionselected');
  $('#divname').hide();

//  $(this).parent().closest('td').addClass("channelselected");



  if ($(this).attr('id') == 'Connector') {
    $(this).addClass('optionselected');
    $('#connectorDetail').hide();
    $('#RPOBlist').show();
    $('#connectorlogin').hide();
    $('#form-sync').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#form-activeIn').hide();
    $('#add_act').hide();
    $('#sync-detail').hide();
    $('#form-InvSync').hide();
  }
  if ($(this).attr('id') == 'test1') {
    $('#connectorDetail').show();
    $('#form-sync').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#add_act').hide();
    $('#sync-detail').hide();
    $('#form-InvSync').hide();
  }

  if ($(this).attr('id') == 'Invoice Series') {
    $(this).addClass('optionselected');
    $('#form-invoice').show();
    $('#connectorlogin').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#form-sync').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#add_act').hide();
    $('#sync-detail').hide();
    $('#form-InvSync').hide();
  }
  if ($(this).attr('id') == 'Auto-ship') {
    $(this).addClass('optionselected');
    $('#form-autoship').show();
    $('#form-sync').hide();
    $('#connectorlogin').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#add_act').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#sync-detail').hide();
    $('#form-InvSync').hide();
  }

  if ($(this).attr('id') == 'SyncID') {
    $(this).addClass('optionselected');
    $('#form-sync').show();
    $('#form-autoship').hide();
    $('#RPOBlist').hide();
    $('#connectorlogin').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#add_act').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#sync-detail').hide();
    $('#form-InvSync').hide();
  }


  if ($(this).attr('id') == 'InvSync') {
    $(this).addClass('optionselected');
    $('#form-InvSync').show();
    $('#form-autoship').hide();
    $('#RPOBlist').hide();
    $('#connectorlogin').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#add_act').hide();
    $('#connectorDetail').hide();
    $('#form-activeIn').hide();
    $('#sync-detail').hide();
  }

  if ($(this).attr('id') == 'Sync_Id') {
     $(this).addClass('optionselected');
    $('#connectorDetail').hide();
    $('#sync-detail').show();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#add_act').hide();
    $('#RPOBlist').hide();
    $('#form-InvSync').hide();
  }

  if ($(this).attr('id') == 'ActiveIn') {
    $(this).addClass('optionselected');
    $('#form-sync').hide();
    $('#form-activeIn').show();
    $('#connectorlogin').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#connectorDetail').hide();
    $('#form-autoship').hide();
    $('#add_act').hide();
    $('#sync-detail').hide();
    $('#form-InvSync').hide();
  }
});

// For Radio Buttons [Default] & [Custom] Invoice



$('input[name=Cinvoice]:radio').on("click", function () {
  var name = $(this).data('name');
  var val = $(this).val();
  if (val == 'custom') {
    $('#collbundle').collapse("show");
    $("#form-customseries").show();
    $("#form-defaultseries").hide();
    getcustomInvoiceSeries(our_Aff_id);
  } else if (val == 'default') {
    $('#collbundle').collapse("hide");
    $("#collbundle").removeClass('in');
    $("#form-customseries").hide();
    $("#form-defaultseries").show();
  }
});



//        updatechannelname

function updatechannelname(newName, oriName, myaffid) {
//  alert(newName + oriName + myaffid);

  var formData = {newName: newName, oriName: oriName, myid: myaffid};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
      $('#showerror').html(data);
    },
    error: function (data) {
    }
  });
}



function blockSpecialChar(event) {
  var k = event.keyCode;
  if (!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))) {
    event.returnValue = false;
    return true;
  }
  event.returnValue = true;
}

$('.btn').on('click', function () {
  var $this = $(this);
  $this.button('loading');
  setTimeout(function () {
    $this.button('reset');   // to reset the button
  }, 1000);
});


//getInvoice

function getInvoice() {
  var formData = {affid_getInv: our_Aff_id};
  $.ajax({
    type: 'POST',
    url: filename,
    data: formData,
    cache: true,
    success: function (data) {
//        alert(data);
      $('#inv_data').html(data);
    },
    error: function (data) {
//      alert(data);
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
    }
  });
}

$("body").on('keydown', 'section', function (e) {
  if (e.keyCode == 27) {
    $('#channelbar').hide();
    $('#form-sync').hide();
    $('#divname').hide();
    $('#connectorDetail').hide();
    $('#connectorlogin').hide();
    $('#sync-detail').hide();
    $('#form-invoice').hide();
    $('#form-customseries').hide();
    $('#form-defaultseries').hide();
    $('#form-autoship').hide();
    $('#connectorlogin').hide();
    $('#form-activeIn').hide();
    $('#add_act').hide();
  }
});

function getUserid(val) {
  var str = '#showacc'+val;
  var valnew = $(str);
  var data = {val: val};
//  alert(valnew);
  $.ajax({
    type: 'POST',
    url: filename,
    data: data,
    cache: true,
    success: function (data) {
//        alert(data);
      $(str).html(data);
    },
    error: function (data) {
      alert('error');
      $("#thanks").hide();
      $("#error").show().fadeIn(1000);
    }
  });
}


$("body").on('click', '.common', function commonfunc() {
  $('.common').removeClass('channelselected');
  our_Aff_id = $(this).attr('id');
  hdtext = $(this).text();
  Userid = $(this).data("userid");
  Affname = $(this).data("affname");

  $(this).addClass('channelselected');

//  alert(our_Aff_id+hdtext);
//  alert(hdtext);
  chh_name = $(this).attr('data-chh');

  $('#myhidden').val(our_Aff_id);
  $('#UserNameHidden').val(hdtext);
  $('#UseridHidden').val(Userid);
  $('#AffNameHidden').val(Affname);
  $('#AffIdHidden').val(our_Aff_id);

  UsernameInput = $('#UserNameHidden').val();
  UseridInput = $('#UseridHidden').val();
  AffNameInput = $('#AffNameHidden').val();
  AffIdInput = $('#AffIdHidden').val();

  if (UseridInput == '') {
    UseridInput = "newacc";
  }

//  getConnectorDetail(UsernameInput, UseridInput);
//  alert("Username:  "+UsernameInput+"\n"+"Userid : "+UseridInput+"\n AffName:"+AffNameInput+"\n AffId : "+ our_Aff_id);
getChanneloption(AffIdInput, UsernameInput);  // for getting channel bar option (Connector && Sync)..
  var className = $('.myclass').attr('class');
  affid = $(this).attr('id');
  $('#activeicon').removeClass($('#activeicon').attr('class'));

  $('#channelbar').show();
  $('#RPOBlist').hide();
  $('#form-sync').hide();
  $('#divname').hide();
  $('#connectorDetail').hide();
  $('#connectorlogin').hide();
  $('#sync-detail').hide();
  $('#form-invoice').hide();
  $('#form-customseries').hide();
  $('#form-defaultseries').hide();
  $('#form-autoship').hide();
  $('#connectorlogin').hide();
  $('#form-activeIn').hide();
  $('#add_act').hide();
  $('#form-InvSync').hide();
  $('a').removeClass('linkactive');

});

$('#RPOBlist').on('click', '#apobSave', function () {
//  alert('apobSave');

  //            getConnectorDetail('qwertyuu', '1');

  $('#connectorDetail').show();
//  getChannelDetail('Connector');
//  getConnectorDetail(UsernameInput, UseridInput);

  var apob = $('#APOBlist').val();
  if (apob !== '') {

    $('#connectorDetail').show();
    getnewaccount();
// var data = {apobVAL: apob, affiliateid: our_Aff_id, uidforchn: hdtext};
//      alert(valnew);
//    $.ajax({
//      type: 'POST',
//      url: 'getConnectorData.php',
//      data: data,
//      cache: true,
//      success: function (data) {
//        alert('Updated..');
//        sendconnector();
//      $('#RPOBoptions').html(data);
//      },
//      error: function (data) {
//        alert('error');
//        $("#thanks").hide();
//        $("#error").show().fadeIn(1000);
//      }
//    });

  } else {
    $('#connectorDetail').hide();
  }
//  alert(apob);
});

//Connector Label





$('body').on('click', '#GotoLogin', function () {

  var UsernameInput = $('#UserNameHidden').val();
  var UseridInput = $('#UseridHidden').val();
  $('#connectorDetail').show();
//  getChannelDetail('Connector');
  getConnectorDetail(UsernameInput, UseridInput);

});




//send connector

$('body').on('click', '#Connector', function sendconnector(e1,e2,e3) {
  var apob = $('#APOBlist').val();
//alert(apob);
  if (apob != null) {
//    $('#connectorDetail').show();
    getConnectorDetail(UsernameInput, UseridInput);
  } else {
    $('#connectorDetail').hide();
  }
//  alert('apob');
//  getConnectorDetail('qwertyuu', '1');
  var UserNameInput = $('#UserNameHidden').val();
  hdval = $('#myhidden').val();
  var callingAPOB = "kaleem";
  var data = {callingAPOB: callingAPOB, affiliateid: hdval, uidforchn: UserNameInput};
//  alert(valnew);
  $.ajax({
    type: 'POST',
    url: 'getConnectorData.php',
    data: data,
    cache: true,
    success: function (data) {
    $(".RPOBoptions").html(data);
//  alert(data);
//  sendconnector();
//  $('').html(data);
    },
    error: function (data) {
      alert('error');
      $("#error").show().fadeIn(1000);
    }
  });
});


function getnewaccount() {
//  alert('Im getnewaccount()')
  datastring1 = {getnewaccount: hdval};
  $.ajax({
    type: 'POST',
    url: 'getConnectorData.php',
    data: datastring1,
    cache: true,
    success: function (data) {
//        alert(data);
      $('#showAccount').html(data);
    },
    error: function (data) {
      alert('error');
      $("#error").show().fadeIn(1000);
    }
  });
}


$('body').change('#APOBlist', function () {

  if ($('#APOBlist').val() == '') {
    $('#apobSave').attr('disabled', true);
  } else {
    $('#apobSave').attr('disabled', false);

    SelectApob = $('#APOBlist').val();
//  SelectApob =  $('#apobHidden').val(SelectApob);
//  SelectApob =  $('#apobHidden').val();
//     alert(SelectApob);
//      apobHidden

  }

});
