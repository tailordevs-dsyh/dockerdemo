<?php
/* DB Functions START */

function good_query($string, $debug = 0) {
    if ($debug == 1)
        print $string;

    if ($debug == 2)
        error_log($string);

    if (!mysql_ping($GLOBALS['db_handle'])) {
        mysql_close($GLOBALS['db_handle']);
        global $db_handle, $db_found;
        $db_handle = mysql_connect($GLOBALS['server'], $GLOBALS['user_name'], $GLOBALS['password']);
        $db_found = mysql_select_db($GLOBALS['database'], $db_handle);
//        print_r($db_handle);
    }

    $result = mysql_query($string);

    if ($result == false) {
        error_log("SQL error: " . mysql_error() . "\n\nOriginal query: $string\n");
        if ($_SESSION['lname'] == 'hari' || $_SESSION['lname'] == 'ravi' || $_SESSION['lname'] == 'Cron' || $_SESSION['lname'] == 'System' || $_SESSION['lname'] == 'CronRTO' || $_SESSION['lname'] == 'dhrupad') {
            print ("SQL error: " . mysql_error() . "\n\nOriginal query: $string\n");
        }
        if ($_SESSION[login] == 1) {
            //echo 'Something went wrong, else please login again';
            die('Something went wrong, else please login again');
        }
        // Remove following line from production servers
        //die("SQL error: " . mysql_error() . "\b<br>\n<br>Original query: $string \n<br>\n<br>");
    }
    //mysql_close();
    return $result;
}

function good_query_list($sql, $debug = 0) {
    // this function require presence of good_query() function
    $result = good_query($sql, $debug);

    if ($lst = mysql_fetch_row($result)) {
        mysql_free_result($result);
        return $lst;
    }
    mysql_free_result($result);
    return false;
}

function good_query_assoc($sql, $debug = 0) {
    // this function require presence of good_query() function
    $result = good_query($sql, $debug);

    if ($lst = mysql_fetch_assoc($result)) {
        mysql_free_result($result);
        return $lst;
    }
    mysql_free_result($result);
    return false;
}

function good_query_value($sql, $debug = 0) {
    // this function require presence of good_query_list() function
    $lst = good_query_list($sql, $debug);
    return is_array($lst) ? $lst[0] : false;
}

function good_query_table($sql, $debug = 0) {
    // this function require presence of good_query() function
//    echo $GLOBALS['server'].' , '.$GLOBALS['user_name'].' , '.$GLOBALS['password'];
    $result = good_query($sql, $debug);

    $table = array();
    if (mysql_num_rows($result) > 0) {
        $i = 0;
        while ($table[$i] = mysql_fetch_assoc($result))
            $i++;
        unset($table[$i]);
    }
    mysql_free_result($result);
    return $table;
}

/* DB Function END */
